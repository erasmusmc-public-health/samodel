/*
This file is part of NTDSim, for the Gates NTD Impact project at the Erasmus
Medical Center in Rotterdam.
Within the theme Health Sciences the department of Public Health does research
into various subjects within the health care. The main objective of the
department is to conduct eminent research and provide excellent education with
a discernable impact on population health at the local, national, and
international level. 
This project involves the development of a simulation suite with individual
based models for the transmission and control of Neglected Tropical Diseases. 

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
by applicable law or agreed to in writing, software distributed under the
License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
OF ANY KIND, either express or implied. See the License for the specific
language governing permissions and limitations under the License.

 */
package SIS;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Dummy test class.
 *
 * @author rinke
 *
 */
public class DummyTest {

    @Test
    public void test() {
        assertTrue(true);
    }

}

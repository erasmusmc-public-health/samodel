package subPopulationGeneral;

import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.eventmanager.IEventManager;
import strains.StrainModel;

/**
 * Event that can be scheduled in the eventmanager, to execute a transmission event between sub-population members. 
 *  
 * @author roelb
 * 
 */
public class SubPopulationTransmissionEvent extends Event implements IEvent {
	
	private final SubPopulation   	owner;
    private final StrainModel 		strainmodel;

    public SubPopulationTransmissionEvent(SubPopulation subPopulation, StrainModel strainmodel, IEventManager eventManager) {
        super(eventManager);
        this.strainmodel = strainmodel;
        owner = subPopulation;
    }
    
    /**
     * Infection event execution, refers back to the household of the newly infected to handle within household effects. 
     */
    @Override
    public void execute() {
        owner.handle(this);
    }

	public StrainModel getStrainmodel() {
		return strainmodel; //needed by the household to know the infecting strain
	}
}

package subPopulationGeneral;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import antibiotics.AntibioticStatesEnum;
import hospitals.HospitalStatesEnum;
import multiCCsimSAbasics.Human;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.math.DistributionType;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import strains.StrainModel;
import strains.StrainStatesEnum;
import strains.StrainTreeElement;

/**
 * Sub-Population class, deals with within sub-population transmission events. Also holds extra detailed individual level
 * output, and allows querying members status for output purposes.
 *
 * @author roelb
 * @author Anneke S. de Vos
 *
 */
public abstract class SubPopulation {

    @FunctionalInterface
    interface ContactRateFormula {
        double compute(double rawRate, double alpha, int nMembers);
    }

    /**
     * The sub-population contact rate level of variation has three options.
     * Either all individuals within a sub-population have the same rate of contacting each other,
     * or variation is at the level of individuals. In the latter case, contacts may include indirect
     * contacts with oneself, or not.
     */
    public static enum SubPopulationContactRateLevelOfVariation {
        /**
         */
        SUBPOPULATION,
        /**
         */
        SUBPOPULATIONMEMBERSC,
        /**
         */
        SUBPOPULATIONMEMBERNOSC;
    }

    /**
     * The sub-population contact structure has three options.
     * The mean number of daily sub-population contacts per sub-population is linear with, constant (independent of) or
     * saturating with the number of sub-population members.
     * In all three options, the contactrate is sub-population specific, and infectivity is individual.
     */
    public static enum SubPopulationContactRateStructuring implements ContactRateFormula {
        /**
         * transmission rate (per sub-population) = contactrate * Sum(beta_i) * S
         */
        LINEAR((rate, alpha, n) -> rate),
        /**
         * transmission rate (per sub-population) = contactrate * Sum(beta_i) * S / (N - 1)
         */
        CONSTANT((rate, alpha, n) -> rate / (n - 1)),
        /**
         * transmission rate (per sub-population) = contactrate * Sum(beta_i) * S * alfa * log(N) / N, where
         * alfa is a tuning parameter (for example if alfa 1/log(2)~1.4427, the contact rate per person is multiplied
         * by 1 for N=2, by 1.585 for N=3, by 2 for N=4 etc.)
         */
        SATURATING((rate, alpha, n) -> rate * alpha * Math.log(n) / n);

        private ContactRateFormula formula;

        SubPopulationContactRateStructuring(ContactRateFormula formula) {
            this.formula = formula;
        }

        @Override
        public double compute(double rawRate, double alpha, int nMembers) {
            return formula.compute(rawRate, alpha, nMembers);
        }
    }

    protected final int                                       iD;
    protected final List<Human>                               members;
    protected final World                                     world;

    protected SubPopulationTypeEnum                           whichContactRate;           // enum is used to get the correct human contact rate
    protected SubPopulationContactRateLevelOfVariation        contactRateLevelOfVariation;
    protected SubPopulationContactRateStructuring             contactRateStructure;
    protected double                                          alfa;
    protected int                                             nrOfMembers = 0;
    protected final double                                    contactrate;                // sub-population specific contactrate.
    protected final Map<String, double[]>                     susceptibilities;

    private final Map<String, SubPopulationTransmissionEvent> transmissionEvents;
    private final StringBuilder                               sPOutPut;                   // SubPopulation level detailed output; who gets infected or
                                                                                          // cured, when

    public SubPopulation(World world, double contactrate, int iD) {
        this.world = world;
        this.contactrate = contactrate;
        this.iD = iD;
        members = new ArrayList<>();
        susceptibilities = new HashMap<>();
        transmissionEvents = new HashMap<>();
        for (final String strainName : world.getStrainmodels().getModels().keySet()) {
            transmissionEvents.put(strainName,
                    new SubPopulationTransmissionEvent(this, world.getStrainmodels().getModels().get(strainName),
                            world.getEventManager()));
        }
        sPOutPut = new StringBuilder();
    }

    /**
     * Part one of the sub-population daily transmission rate calculation, depends on the level of variation in daily
     * contact rates. Also stores for each individual relative contact rate and susceptibility, for later use in drawing
     * of a susceptible who gets infected.
     * @param strain Which SA CC
     * @param whichContactRate Which subpopulation contact-rate.
     * @param whichInfandSus Which infection and susceptibility rate to use (taking antibiotics usage into account or not)
     * @return The sub-population transmission rate assuming linear increase in contacts with sub-population members.
     */
    public double calculateRawTransmissionRateFromVariatonLevel(String strain, SubPopulationTypeEnum whichContactRate,
            AntibioticStatesEnum whichInfandSus) {
        double rate = 0.;
        double suminf = 0.;
        double sumsus = 0.;
        double sumcontact = 0.;
        final double[] newSusceptibilities = new double[nrOfMembers];
        int i = 0;
        switch (contactRateLevelOfVariation) {
            case SUBPOPULATION:
                for (final Human human : members) {
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.I) {
                        newSusceptibilities[i] = 0.0;
                        suminf += human.getStrains().get(strain).getInfectivity(whichInfandSus);
                    }
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
                        newSusceptibilities[i] = human.getStrains().get(strain).getSusceptibility(whichInfandSus);
                        sumsus += newSusceptibilities[i];
                    }
                    i++;
                }
                rate = contactrate * suminf * sumsus;
                break;
            case SUBPOPULATIONMEMBERSC:
                for (final Human human : members) {
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.I) {
                        newSusceptibilities[i] = 0.0;
                        suminf += human.getStrains().get(strain).getInfectivity(whichInfandSus)
                                * human.getContactrateSubPop(whichContactRate);
                        sumcontact += human.getContactrateSubPop(whichContactRate);
                    }
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
                        newSusceptibilities[i] = human.getStrains().get(strain).getSusceptibility(whichInfandSus)
                                * human.getContactrateSubPop(whichContactRate);
                        sumsus += newSusceptibilities[i];
                        sumcontact += human.getContactrateSubPop(whichContactRate);
                    }
                    i++;
                }
                rate = nrOfMembers * suminf * sumsus / sumcontact;
                break;
            case SUBPOPULATIONMEMBERNOSC:
                for (final Human human : members) {
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.I) {
                        newSusceptibilities[i] = 0.0;
                        suminf += human.getStrains().get(strain).getInfectivity(whichInfandSus)
                                * human.getContactrateSubPop(whichContactRate);
                        sumcontact += human.getContactrateSubPop(whichContactRate);
                    }
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
                        sumcontact += human.getContactrateSubPop(whichContactRate);
                    }
                    i++;
                }
                i = 0;
                for (final Human human : members) {
                    if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
                        newSusceptibilities[i] = human.getStrains().get(strain).getSusceptibility(whichInfandSus)
                                * human.getContactrateSubPop(whichContactRate) *
                                suminf / (sumcontact - human.getContactrateSubPop(whichContactRate));
                        sumsus += newSusceptibilities[i];
                    }
                    i++;
                }
                rate = (nrOfMembers - 1) * sumsus;
                break;
        }
        susceptibilities.put(strain, newSusceptibilities);
        return rate;
    }

    public double fractionPatientsToPersonnel() {
        int patients = 0;
        int personnel = 0;
        for (final Human human : members) {
            if (human.getAntibioticsAndHospitalState().getHospitalState() == HospitalStatesEnum.HOSPITAL_PERSONNEL) {
                personnel++;
            } else {
                patients++;
            }
        }
        return (double) patients / personnel;
    }

    public String getHhOutPut() {
        return sPOutPut.toString();
    }

    public int getID() {
        return iD;
    }

    /**
     * Weighted by relative contact rate and susceptibility of individuals, as stored in the infection rate calculation,
     * random selection of a susceptible in a sub-population.
     * @param strainmodel Which SA CC.
     * @return Who is chosen.
     */
    private Human getRandomSusceptible(StrainModel strainmodel) {
        final int random = world.getMath().getDiscreteDistribution(DistributionType.CUSTOM,
                null, null, susceptibilities.get(strainmodel.getName())).nextInt();
        return members.get(random);
    }

    /**
     * Calculates the within sub-population daily rate of transmission, depending on the states of the members, and
     * on the transmission model.
     * @param strain Which SA CC.
     * @param whichContactRate Which contact-rate to calculate with? What subgroup's rate, and with or without hospitalization effects?
     * @param whichInfandSus Which infectivity and susceptibility to calculate with? With or without current antibiotics usage effects?
     * @return The sub-population transmission rate.
     */
    protected double getSubPopulationTransmissionRate(String strain, SubPopulationTypeEnum whichContactRate,
            AntibioticStatesEnum whichInfandSus) {
        if (nInState(strain, StrainStatesEnum.I) == 0 || nInState(strain, StrainStatesEnum.S) == 0) {
            return 0.;
        }
        double rawRate = calculateRawTransmissionRateFromVariatonLevel(strain, whichContactRate, whichInfandSus);
        return contactRateStructure.compute(rawRate, alfa, nrOfMembers);
    }

    protected void handle(StrainModel sme) {
        final Human human = getRandomSusceptible(sme);
        human.getStrains().get(sme.getName()).infect(whichContactRate.getDescription());
    }

    /**
     * A sub-population infection event is handled.
     * @param e The transmission event.
     */
    public void handle(SubPopulationTransmissionEvent e) {
        handle(e.getStrainmodel());
    }

    /**
     * Creates a list of all sub-population members in a specific state.
     * @param strain Which SA CC.
     * @param state Which infection state.
     * @return A list of all sub-population members in a specific state.
     */
    private List<Human> inState(String strain, StrainStatesEnum state) {
        final List<Human> selected = new ArrayList<>();
        for (final Human human : members) {
            if (human.getStrains().get(strain).getState() == state) {
                selected.add(human);
            }
        }
        return selected;
    }

    private List<Human> inState(String strain, StrainStatesEnum state, boolean isNursingHomePersonnel) {
        final List<Human> selected = new ArrayList<>();
        for (final Human human : members) {
            if (human.getStrains().get(strain).getState() == state && human.isNursingHomePersonnel() == isNursingHomePersonnel) {
                selected.add(human);
            }
        }
        return selected;
    }

    private List<Human> inState(String strain, StrainStatesEnum state, HospitalStatesEnum hospState) {
        final List<Human> selected = new ArrayList<>();
        for (final Human human : members) {
            if (human.getStrains().get(strain).getState() == state
                    && human.getAntibioticsAndHospitalState().getHospitalState() == hospState) {
                selected.add(human);
            }
        }
        return selected;
    }

    /**
     * Adds a human to a sub-population.
     * @param human Which human.
     */
    public void join(Human human) {
        members.add(human);
        nrOfMembers++;
    }

    /**
     * Removes a human from a sub-population.
     * @param human Which human.
     */
    public void leave(Human human) {
        members.remove(human);
        nrOfMembers--;
    }

    public int nInfected(String strain) {
        return nInState(strain, StrainStatesEnum.I);
    }

    public int nInfected(String strain, boolean isNursingHomePeronnel) {
        return nInState(strain, StrainStatesEnum.I, isNursingHomePeronnel);
    }

    public int nInfected(String strain, HospitalStatesEnum hospState) {
        return nInState(strain, StrainStatesEnum.I, hospState);
    }

    /**
     * Returns the number of sub-population members in a specific state.
     * @param strain Which SA CC.
     * @param state Which infection state.
     * @return The number of sub-population members in the specified state.
     */
    private int nInState(String strain, StrainStatesEnum state) {
        return inState(strain, state).size();
    }

    private int nInState(String strain, StrainStatesEnum state, boolean isNursingHomePeronnel) {
        return inState(strain, state, isNursingHomePeronnel).size();
    }

    private int nInState(String strain, StrainStatesEnum state, HospitalStatesEnum hospState) {
        return inState(strain, state, hospState).size();
    }

    public int nUninfected(String strain) {
        return nInState(strain, StrainStatesEnum.S);
    }

    public int nUninfected(String strain, boolean isNursingHomePeronnel) {
        return nInState(strain, StrainStatesEnum.S, isNursingHomePeronnel);
    }

    public int nUninfected(String strain, HospitalStatesEnum hospState) {
        return nInState(strain, StrainStatesEnum.S, hospState);
    }

    /**
     * Sub-Population level event data is written to the stringbuilder
     * @param newHumanState the new state
     * @param humanID which human
     * @param strain which strain
     * @param how how did the state change occur.
     */
    public void SPoutput(String newHumanState, long humanID, String strain, String how) {
        sPOutPut.append(TimeUtilsDays.timePointAsDays(world.getEventManager().now()) - world.getStartTimeAsDay());
        sPOutPut.append(",").append(iD);
        sPOutPut.append(",").append(members.size());
        sPOutPut.append(",").append(humanID);
        sPOutPut.append(newHumanState);
        sPOutPut.append(strain);
        sPOutPut.append(how);
    }

    /**
     * Starts up a sub-population, after humans have been made for it, starting them first.
     */
    public void startSubpopulation() {
        for (final Human human : members) {
            human.startHuman();
        }
        for (final String strainName : world.getStrainmodels().getModels().keySet()) {
            final double[] susceptibles = new double[nrOfMembers];
            susceptibilities.put(strainName, susceptibles);
        }
    }

    /*
     * Test all members for MRSA.
     */
    public void testHouseholdForMRSA() {
        for (final Human human : members) {
            human.getAntibioticsAndHospitalState().getHospitalProcessor().mrsaTestResultEventForFamily();
        }
    }

    public void updateInfectionRate(StrainTreeElement strain) {
        updateInfectionRate(strain.getStrainModel().getName());
    }

    /**
     * Change occurs, i.e. infection or curing of a sub-population member, or a member takes Antibiotics,
     * which affects their susceptibility, so that the next infection event needs to be rescheduled (or
     * scheduled, or cancelled) accordingly.
     * @param strain Which SA CC.
     * @param humanID Which human was affected? For extra output purposes only.
     * @param how How did infection or clearing occur? For extra output purposes only.
     * @param newHumanState Infection, clearing, or AB? For extra output purposes only.
     */
    public void updateInfectionRate(StrainTreeElement strain, long humanID, String newHumanState, String how) {
        updateInfectionRate(strain.getStrainModel().getName());
        SPoutput(newHumanState, humanID, strain.getStrainModel().getName(), how);
    }

    public void updateInfectionRate(String strain) {
        final SubPopulationTransmissionEvent e = transmissionEvents.get(strain);
        final double rate = getSubPopulationTransmissionRate(strain, whichContactRate, AntibioticStatesEnum.NOT_TAKING_AB);
        final double randomtime = world.nextExp(1 / rate);
        if (randomtime <= world.getStopTimeAsDay()) {
            e.cancel();
            e.scheduleRelative(TimeUtilsDays.asDuration(randomtime)); 
        } else {
            e.cancel();
        }
    }

}

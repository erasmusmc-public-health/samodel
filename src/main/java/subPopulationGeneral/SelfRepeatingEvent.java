package subPopulationGeneral;

import inandoutput.ParameterInput;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.math.IMath;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;

/**
 * Template for selfrepeating events, each event schedules the next using the EventManager.
 * Based on Roel's Population Infection event class.
 * @author Anneke S. de Vos
 */
public abstract class SelfRepeatingEvent extends Event implements IEvent {
	
	private static class Input {
		/*
		 * waitPeriod in days is needed for a comparison with the obtained time to next event in years (comparison after 
		 * conversion to model time instead can lead to number overflow within timeUtils, as timeUtils has a max date)
		 */
	    private final static double waitPeriodInDays = ParameterInput.getInstance().getEventsWaitPeriodInDays(); 
	    /*
	     * WaitPeriod in event manager system time.
	     */
	    private final static long   WAITPERIOD        = TimeUtilsDays.asDuration(waitPeriodInDays);  
	}
    private final IMath         math;
    private boolean             waiting;

    public SelfRepeatingEvent(World world) {
        super(world.getEventManager());
        waiting = true;
        math = world.getMath();
    }

    /**
     * Override with the event to schedule
     */
    protected abstract void eventToSchedule();

    /**
     * An event is executed, and the next event is scheduled based on sumRates (the summed rate of infection /
     * hospitalisation /antibiotics uptake etc.) In case the time to the next start event exceeds the WAITPERIOD,
     * the next event be scheduled after WAITPERIOD (so the simulated world won't have changed too much meanwhile,
     * which would invalidate the calculated time to next event).
     * @author roelb
     */
    @Override
    public void execute() {
        if (!waiting) {
            eventToSchedule();
        }
        final double sumrates = sumRates();
        if (sumrates > 0) {
            final double dt = math.nextExp(1 / sumrates);
            if (dt < Input.waitPeriodInDays) {
                final long longDt = TimeUtilsDays.asDuration(dt); 
                waiting = false;
                scheduleRelative(longDt);
                return;
            }
        }
        waiting = true;
        scheduleRelative(Input.WAITPERIOD);
    }
    
    /**
     * To Override with the summed rates, i.e. from a magic tree
     * @return summed event rates
     */
    protected abstract double sumRates();

}

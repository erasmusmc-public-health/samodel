package subPopulationGeneral;

import strains.HowEnum;

/*
 * This enum enables the sub-population to calculate the infection-rate both with and without taking into account
 * possible antibiotics usage of its members. It is used to get a specific contact rate from a Human.
 * The String description is for use in the extra output.
 */
public enum SubPopulationTypeEnum {
	HOUSEHOLD (HowEnum.HOUSEHOLD),
	NURSINGHOME (HowEnum.NURSINGHOME),
	HOUSEHOLD_BUT_HOSPITALISED(HowEnum.HOSPITAL),
	NURSINGHOME_BUT_HOSPITALISED(HowEnum.HOSPITAL),
	HOSPITAL (HowEnum.HOSPITAL);
	
    private final HowEnum description;

    private SubPopulationTypeEnum(HowEnum description) {
        this.description = description;
    }

    public HowEnum getDescription() {
        return description;
    }
}

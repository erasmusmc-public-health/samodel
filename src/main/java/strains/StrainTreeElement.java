/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strains;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import antibiotics.AntibioticStatesEnum;
import hospitals.HospitalStatesEnum;
import inandoutput.ParameterInput;
import multiCCsimSAbasics.Human;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import util.mtree.ITreeElement;
import util.mtree.TreeElement;

/**
 * Defines the SA CC strains and their interaction with humans.
 * Indexes (flags) 0 and 1 represent the uninfected and infected states respectively.
 * Rates 0 and 1 represent the internal population susceptibility and infectiousness. The third tree rate represents
 * susceptibility to the additional external force of infection.
 * @author roelb and Anneke de Vos
 */
public class StrainTreeElement extends TreeElement implements ITreeElement {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(World.class);

    private final static int    treeIndexSeFOI   = 2;     // third tree rate = external force of infection susceptible exposure rate
    // rates 0 and 1 respectively are internal population susceptibility and infectiousness, i.e. in order with the flags S and I
    
	private static class Input {
	    private final static double generalPopulationContactRateMultiplierForHospitalised = ParameterInput.getInstance()
	            .getGeneralPopulationContactRateMultiplierForHospitalised();
	    private final static double afterABsusceptibilityMultiplier                       = ParameterInput.getInstance()
	            .getAfterAntibioticsSusceptibilityMultiplier();
	}
    private StrainStatesEnum    state;
    private CureEvent           cureEvent                                             = null;
    private double              contactratePop;
    private final double        externalForceOfInfectionExposure;
    private final double        infectivity;
    private double              susceptibility;
    private final StrainModel   strainModel;
    private final Human         human;

    public StrainTreeElement(Human human, StrainModel strainmodel) {
        super(human.getHumanNr(), StrainStatesEnum.values().length, StrainStatesEnum.values().length + 1); // 2 states (S and I), and 3 rates
        // (individual internal population susceptibility and infectiousness, and susceptibility from external sources)
        this.human = human;
        strainModel = strainmodel;
        cureEvent = new CureEvent(this, human.getWorld().getEventManager());
        state = StrainStatesEnum.S;
        infectivity = human.getInfectiousness() * strainmodel.getInfectivity();
        susceptibility = human.getSusceptibility();
        contactratePop = human.getContactratePop();
        externalForceOfInfectionExposure = (human.isIneFOIRiskgroup() ? 1 : 0) * strainmodel.getExternalForceOfInfection();
        final int k = state.ordinal();
        setFlagExclusive(k);
        if (state == StrainStatesEnum.S) {
            setRateExclusive(contactratePop * susceptibility, k);
            setRate(externalForceOfInfectionExposure * susceptibility, treeIndexSeFOI);
        } else if (state == StrainStatesEnum.I) {
            setRateExclusive(contactratePop * infectivity, k);
        }
        setContactRate(contactratePop);
    }

    private void antibioticsCure() {
        cureEvent.cancel();
        strainModel.increaseNcleared(HowEnum.ANTIBIOTICS);
        curecase();
        if (human.isGiveExtraOutput()) {
            caseUpdateHH(",cleared,", ",AB\n");
        } else {
            caseUpdateHH();
        }
        if (human.isNursingHomePersonnel()) {
            if (human.isGiveExtraOutputExtraHousehold()) {
                caseUpdateExtraHH(",cleared,", ",AB\n");
            } else {
                caseUpdateExtraHH();
            }
        }
    }

    /**
     * At the end of antibiotic therapy, an individual may be cured of SA.
     * @param targetedTreatmentIsOn Is the antibiotics treatment targeted against MRSA, increasing the clearing probability?
     */
    public void antibioticsPossibleCure(boolean targetedTreatmentIsOn) {
        final double clearingProbability = targetedTreatmentIsOn ? strainModel.getClearByTargetedABProb()
                : strainModel.getClearByABProb();
        if (human.getWorld().nextDouble() < clearingProbability) {
            antibioticsCure();
        }
    }

    public void caseUpdateExtraHH() {
        human.getExtraHousehold().updateInfectionRate(this);
    }

    public void caseUpdateExtraHH(String newHumanState, String how) {
        human.getExtraHousehold().updateInfectionRate(this, human.getHumanNr(), newHumanState, how);
    }

    public void caseUpdateHH() {
        human.getHousehold().updateInfectionRate(this);
    }

    public void caseUpdateHH(String newHumanState, String how) {
        human.getHousehold().updateInfectionRate(this, human.getHumanNr(), newHumanState, how);
    }

    /**
     * Infection state is set to S.
     */
    private void clear() {
        state = StrainStatesEnum.S;
        human.setOverallInfStateS();
    }

    /**
     * If cured, infection is cleared, and the individuals household infection rate changes (as well as possibly the hospital's).
     */
    public void curecase() {
        clear();
        updateFlagsRatesTree();
        if (human.getAntibioticsAndHospitalState().getHospitalState() != HospitalStatesEnum.NOT_HOSPITALISED) {
            human.getWorld().getHospitals().get(human.getAssignedHospital()).updateInfectionRate(this);
        }
    }

    public Human getHuman() {
        return human;
    }

    /**
     * This enables the sub-population to calculate the infection-rate both with and without taking into account
     * possible antibiotics usage of its members.
     * @param whichInfectiousness AntibioticStatesEnum.NOT_TAKING_AB: ignoring AB status, AntibioticStatesEnum.TAKING_AB:
     * taking possible AB use into accounts
     * @return Infectiousness of the individual
     */
    public double getInfectivity(AntibioticStatesEnum whichInfectiousness) {
        switch (whichInfectiousness) {
            case NOT_TAKING_AB: // ignoring AB use
                return state == StrainStatesEnum.I ? infectivity : 0.;
            case TAKING_AB: // taking possible AB use into account
                return (state == StrainStatesEnum.I ? infectivity : 0.)
                        * (human.isUsingAntibiotics() ? strainModel.getaBinfectivityMultiplier() : 1.);
            default:
                LOGGER.error("error: use of undefined AB status");
                return 0.0;
        }
    }

    public StrainStatesEnum getState() {
        return state;
    }

    public StrainModel getStrainModel() {
        return strainModel;
    }

    /**
     * This enables the sub-population to calculate the infection-rate both with and without taking into account
     * possible antibiotics usage of its members.
     * @param whichSusceptibility AntibioticStatesEnum.NOT_TAKING_AB: ignoring AB status, AntibioticStatesEnum.TAKING_AB:
     * taking possible AB use into account
     * @return Susceptibility of the individual
     */
    public double getSusceptibility(AntibioticStatesEnum whichSusceptibility) {
        switch (whichSusceptibility) {
            case NOT_TAKING_AB: // ignoring AB use
                return state == StrainStatesEnum.S ? susceptibility : 0.;
            case TAKING_AB: // taking possible AB use into account
                return (state == StrainStatesEnum.S ? susceptibility : 0.)
                        * (human.isUsingAntibiotics() ? strainModel.getaBsusceptibilityMultiplier() : 1.);
            default:
                LOGGER.error("error: use of undefined AB status");
                return 0.0;
        }
    }

    public void handle(CureEvent e) {
        strainModel.increaseNcleared(HowEnum.NATURAL);
        curecase();
        if (human.isGiveExtraOutput()) {
            caseUpdateHH(",cleared,", ",natc\n");
        } else {
            caseUpdateHH();
        }
        if (human.isNursingHomePersonnel()) {
            if (human.isGiveExtraOutputExtraHousehold()) {
                caseUpdateExtraHH(",cleared,", ",natc\n");
            } else {
                caseUpdateExtraHH();
            }
        }
    }

    /**
     * Infection occurs, the infection counter is increased, and a cure event is scheduled.
     * @param how How did infection occur? For (extra) output purposes.
     */
    public void infect(HowEnum how) {
        if (human.getOverallInfState() == InfectionStatesEnum.S) {
            strainModel.increaseNinfected(how);
            state = StrainStatesEnum.I;
            updateFlagsRatesTree();
            final double scheduleTimeRandom = human.getWorld().getDistributions().drawFromCleardurDist();
            cureEvent.scheduleRelative(TimeUtilsDays.asDuration(scheduleTimeRandom * human.getInfectionDurationAverageInDays())); 
            if (strainModel.isMrsa()) {
                human.setOverallInfStateIMrsa();
            } else {
                human.setOverallInfStateISsa();
            }
            if (human.isGiveExtraOutput()) {
                caseUpdateHH(",infected,", how.toString());
            } else {
                caseUpdateHH();
            }
            if (human.isNursingHomePersonnel()) {
                if (human.isGiveExtraOutputExtraHousehold()) {
                    caseUpdateExtraHH(",infected,", how.toString());
                } else {
                    caseUpdateExtraHH();
                }
            }
            if (human.getAntibioticsAndHospitalState().getHospitalState() != HospitalStatesEnum.NOT_HOSPITALISED) {
                human.getWorld().getHospitals().get(human.getAssignedHospital()).updateInfectionRate(this);
            }
        } else {
            strainModel.increaseNpopfailinfected();
        }
    }

    public void startHospitalEffectsOnGPContacts() {
        contactratePop = contactratePop * Input.generalPopulationContactRateMultiplierForHospitalised;
        setContactRate(contactratePop);
        updateFlagsRatesTree();
    }

    /**
     * Effects of currently taking AB are taken into account through use of human.isUsingAntibiotics()
     */
    public void startstopABeffects() {
        updateFlagsRatesTree();
    }

    public void stopABstartAfterABeffects() {
        susceptibility = susceptibility * Input.afterABsusceptibilityMultiplier;
        updateFlagsRatesTree();
    }

    public void stopAfterABeffects() {
        susceptibility = human.getSusceptibility();
        updateFlagsRatesTree();
    }

    public void stopHospitalEffectsOnGPContacts() {
        contactratePop = human.getContactratePop();
        setContactRate(contactratePop);
        updateFlagsRatesTree();
    }

    /**
     * When an infection or SA clearing occurs, or antibotics-use or hospitalization occurs, the tree nodes
     * (potentially infective or susceptible contact rate totals) are updated.
     */
    private void updateFlagsRatesTree() {
        final int k = state.ordinal();
        if (!getFlag(k)) {
            setFlagExclusive(k);
        }
        if (state == StrainStatesEnum.S) {
            setRateExclusive(contactratePop * susceptibility
                    * (human.isUsingAntibiotics() ? strainModel.getaBsusceptibilityMultiplier() : 1.), k);
            setRate(externalForceOfInfectionExposure * susceptibility
                    * (human.isUsingAntibiotics() ? strainModel.getaBsusceptibilityMultiplier() : 1.), treeIndexSeFOI);
        } else if (state == StrainStatesEnum.I) {
            setRateExclusive(
                    contactratePop * infectivity * (human.isUsingAntibiotics() ? strainModel.getaBinfectivityMultiplier() : 1.),
                    k);
        }
        human.getWorld().getTrees().get(strainModel.getName()).update(this);
    }

}

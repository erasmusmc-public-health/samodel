/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strains;

import nl.erasmusmc.mgz.eventmanager.IEvent;
import subPopulationGeneral.SelfRepeatingEvent;
import util.mtree.IElementMagicTree;
import multiCCsimSAbasics.World;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;


/**
 * Event that can be scheduled in the eventmanager, an SA CC strain is transmitted to one person in the population. 
 * Each PopulationTransmissionEvent also schedules the next event of this type, based on current infection and
 * susceptibility rates.
 * @author roelb
 */
public class PopulationTransmissionEvent extends SelfRepeatingEvent implements IEvent {
    
    private final static int 			treeIndexS = StrainStatesEnum.S.ordinal();
    private final static int 			treeIndexI = StrainStatesEnum.I.ordinal();
    private final IElementMagicTree 	tree;
    
    public PopulationTransmissionEvent(World world, String strain) {
        super(world);
        this.tree = world.tree(strain);
    }

    @Override
    public void eventToSchedule() {
    	if (tree.sum(treeIndexS) > 0) {      	
            final StrainTreeElement streeElement = (StrainTreeElement) tree.selectDRandom(treeIndexS);
            streeElement.infect(HowEnum.GENERALPOPULATION);
    	}
    }
    
    @Override
    public double sumRates() {
        final double sumS = tree.sum(treeIndexS); // i.e. sum of (sus_S * c_S)
        final double sumI = tree.sum(treeIndexI); // i.e. sum of (inf_I * c_I)
        return sumS * sumI / tree.totalContactRate();
        // proportional mixing taking into account heterogeneity in sus
        // and of course freq dependence (NOT density dependence) @auth roelb
    }
	   
}

package strains;

/** 
 * For each SA CC, an individual is either in state S (susceptible) or in state I (infected).  
 */
public enum StrainStatesEnum {
	S, // susceptible 
    I; // Infected
}

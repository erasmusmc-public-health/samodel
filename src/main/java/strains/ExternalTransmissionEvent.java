package strains;

import nl.erasmusmc.mgz.eventmanager.IEvent;
import subPopulationGeneral.SelfRepeatingEvent;
import util.mtree.IElementMagicTree;
import multiCCsimSAbasics.World;


/**
 * Event that can be scheduled in the eventmanager, an SA CC strain is transmitted to one person in the population,
 * from external source (e.g. foreign hospital visit). Each ExternalTransmissionEvent also schedules the next event 
 * of this type, based on current infection and susceptibility rates.
 */
public class ExternalTransmissionEvent extends SelfRepeatingEvent implements IEvent {
    
    private final static int    		treeIndexSeFOI = 2; // external force of infection susceptible exposure rate index of tree  
    private final IElementMagicTree 	tree;
    
    public ExternalTransmissionEvent(World world, String strain) {
        super(world);
        this.tree = world.tree(strain);
    }

    @Override
    public void eventToSchedule() {
        if (tree.countFlag(StrainStatesEnum.S.ordinal()) > 0) {
            final StrainTreeElement streeElement = (StrainTreeElement) tree.selectDRandom(treeIndexSeFOI);
            streeElement.infect(HowEnum.eFOI);
        }
    }
    
    @Override
    public double sumRates() {
        final double sumS = tree.sum(treeIndexSeFOI); 
        return sumS * 1;
    }
	   
}

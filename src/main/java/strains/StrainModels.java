/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strains;

import java.util.HashMap;
import java.util.Map;

import inandoutput.ParameterInput;
import nl.erasmusmc.mgz.math.IMath;

/**
 * Container for all instances of StrainModel, i.e. sets the distributions and parameters for the specific SA CC strains.
 * The LinkedHashMap key strainName links the specific strainmodels to their trees, and to  all other strain specific data  
 * kept in HashMaps throughout the model. 
 *
 * @author roelb
 *
 */
public class StrainModels {

    private final Map<String, StrainModel> models;

    public StrainModels(IMath math) {
        models = new HashMap<>();
        for (int i = 0; i < ParameterInput.getInstance().getNumberOfStrains(); i++) {
            final String strainName = "CC-" + i;
            models.put(strainName,
                    new StrainModel(strainName, i));
        }
    }

    public Map<String, StrainModel> getModels() {
        return models;
    }

}

package strains;

/** 
 * An individual is either in state S (susceptible) or in state ISSA (infected with an SSA, =
 *  non-antibiotic resistant SA) or in state IMRSA (infected with an MRSA).  
 */
public enum InfectionStatesEnum {
	S, // susceptible 
    ISSA, // Infected with SSA
    IMRSA; // Infected with MRSA
}
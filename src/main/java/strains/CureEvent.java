package strains;

import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.eventmanager.IEventManager;

/**
 *
 * Event that can be scheduled in the eventmanager, to cure a specfic SA CC strain.
 * @author roelb
 * 
 */
public class CureEvent extends Event implements IEvent {
    private final StrainTreeElement strain;
 
    public CureEvent(StrainTreeElement strain, IEventManager eventManager) {
        super(eventManager);
        this.strain = strain;
    }

    @Override
    public void execute() {
        strain.handle(this);
    }

}

package strains;

/** 
 * enum used in keeping track of how individuals become or how they clear infection
 */
public enum HowEnum {
	START, // infected from model run start-up
	HOUSEHOLD, // infected by their household member
	NURSINGHOME, // infected in their nursing home
	HOSPITAL, // infected in hospital
	GENERALPOPULATION, // infected by a general population contact
	eFOI, // infected from a foreign source
	NATURAL, // cleared naturally
	ANTIBIOTICS; // cleared by antibiotics
	
	  @Override
	  public String toString() {
	    switch(this) {
	      case START: return ",start\n";
	      case HOUSEHOLD: return ",hh\n";
	      case NURSINGHOME: return ",nh\n";
	      case HOSPITAL: return ",ho\n";
	      case GENERALPOPULATION: return ",gp\n";
	      case eFOI: return ",eFOI\n";
	      case NATURAL: return "nat";
	      case ANTIBIOTICS: return "AB";
	      default: throw new IllegalArgumentException();
	    }
	  }
	
}
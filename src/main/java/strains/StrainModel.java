package strains;

import inandoutput.ParameterInput;

/**
 * Class to define an SA CC strain.
 * @author roelb
 *
 */
public class StrainModel {

    private int                           npopfailinfected           = 0;
    private int                           ninfected                  = 0;
    private int                           ninfectedHousehold         = 0;
    private int                           ninfectedNursinghome       = 0;
    private int                           ninfectedHospital          = 0;
    private int                           ninfectedGeneralpopulation = 0;
    private int                           ninfectedeFOI              = 0;
    private int                           ncleared                   = 0;
    private int                           nclearedNat                = 0;
    private int                           nclearedAB                 = 0;
    private final String                  name;
    private final boolean                 isMrsa;
    private final double                  infectivity;
    private final double                  externalForceOfInfection;
    private final double                  clearByABProb;
    private final double                  clearByTargetedABProb;
    private final double                  aBinfectivityMultiplier;
    private final double                  aBsusceptibilityMultiplier;
    private final double                  fractionInitiallyInfected;

    public StrainModel(String name, int i) {
        this.name = name;        // = "CC-" + i;
        this.isMrsa = i > (ParameterInput.getInstance().getNumberOfSusceptibleStrains() - 1);
        this.infectivity = ParameterInput.getInstance().getInfectionProbabilitiesPerStrain()[i];
        this.externalForceOfInfection = ParameterInput.getInstance().getExternalForceOfInfectionPerStrain()[i];
        this.clearByABProb = ParameterInput.getInstance().getClearingByABProbabilitiesPerStrain()[i];
        this.fractionInitiallyInfected = ParameterInput.getInstance().getFractionInitiallyInfectedPerStrain()[i];
        this.clearByTargetedABProb = isMrsa
                ? 1 - (1 - clearByABProb) / ParameterInput.getInstance().getClearingByABProbabilitiesTargetedIncreaseParameter()
                : clearByABProb;
        this.aBinfectivityMultiplier = Math.pow((1 - clearByABProb), ParameterInput.getInstance().getAntibioticsInfectivityPow());
        this.aBsusceptibilityMultiplier = Math.pow((1 - clearByABProb),
                ParameterInput.getInstance().getAntibioticsSusceptibilityPow());
    }

    public double getaBinfectivityMultiplier() {
        return aBinfectivityMultiplier;
    }

    public double getaBsusceptibilityMultiplier() {
        return aBsusceptibilityMultiplier;
    }

    public final double getClearByABProb() {
        return clearByABProb;
    }

    public double getClearByTargetedABProb() {
        return clearByTargetedABProb;
    }

    public final double getExternalForceOfInfection() {
        return externalForceOfInfection;
    }

    public final double getInfectivity() {
        return infectivity;
    }

    public final String getName() {
        return name;
    }

    public int getNcleared() {
        return ncleared;
    }

    public int getNclearedAntibiotics() {
        return nclearedAB;
    }

    public int getNclearedNaturally() {
        return nclearedNat;
    }

    public int getNinfected() {
        return ninfected;
    }

    public int getNinfectedeFOI() {
        return ninfectedeFOI;
    }

    public int getNinfectedGeneralpopulation() {
        return ninfectedGeneralpopulation;
    }

    public int getNinfectedHospital() {
        return ninfectedHospital;
    }

    public int getNinfectedHousehold() {
        return ninfectedHousehold;
    }

    public int getNinfectedNursinghome() {
        return ninfectedNursinghome;
    }

    public int getNpopfailinfected() {
        return npopfailinfected;
    }

    public void increaseNcleared(HowEnum how) {
        ncleared++;
        switch (how) {
        	case NATURAL:
        		nclearedNat++;
        		break;
        	case ANTIBIOTICS:
        		nclearedAB++;
        		break;
        	default: 
        }
    }

    public void increaseNinfected(HowEnum how) {
        ninfected++;
        switch (how) {
    	case HOUSEHOLD:
    		ninfectedHousehold++;
    		break;
    	case NURSINGHOME:
    		ninfectedNursinghome++;
    		break;
    	case HOSPITAL:
    		ninfectedHospital++;
    		break;
    	case GENERALPOPULATION:
    		ninfectedGeneralpopulation++;
    		break;
    	case eFOI:
    		ninfectedeFOI++;
    		break;
    	default: 
        }
    }

    public void increaseNpopfailinfected() {
        npopfailinfected++;
    }

    public boolean isMrsa() {
        return isMrsa;
    }

	public double getFractionInitiallyInfected() {
		return fractionInitiallyInfected;
	}

}
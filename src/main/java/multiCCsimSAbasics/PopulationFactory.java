/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiCCsimSAbasics;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hospitals.HospitalPopulation;
import hospitals.HospitalStatesEnum;
import inandoutput.ParameterInput;
import nl.erasmusmc.mgz.utils.process.impl.InitializableProcess;
import nl.erasmusmc.mgz.utils.process.model.IInitializableProcess;
import subPopulationGeneral.SubPopulation;

/**
 * Class to generate households and nursing homes, and thereby humans, and hospitals.
 * @author roelb
 * @author Anneke S. de Vos
 */
public class PopulationFactory extends InitializableProcess implements IInitializableProcess {

    private static final Logger            LOGGER         = LoggerFactory.getLogger(PopulationFactory.class);
	private static class Input {
	    private final static int               numberOfHouseholds                        = ParameterInput.getInstance()
	            .getNumberOfHouseholds();
	    private final static int               numberOfNursingHomes                      = ParameterInput.getInstance()
	            .getNumberOfNursingHomes();
	    private final static int[]             nhSizes                                   = ParameterInput.getInstance()
	            .getNursingHomeSizeOptions();
	    private final static double[]          nhSizeProbabilities                       = ParameterInput.getInstance()
	            .getNursingHomeSizeProbabilitiesNormalised();
	    private final static double[]          hhSizeProbabilities                       = ParameterInput.getInstance()
	            .getHouseholdSizeProbabilitiesNormalised();
	    private final static int               numberOfHospitals                         = ParameterInput.getInstance()
	            .getNumberOfHospitals();
	    private final static double            numberOfPersonnelPerNursingHomeInhabitant = ParameterInput.getInstance()
	            .getNumberOfPersonnelPerNursingHomeInhabitant();
	    private final static double            numberOfPersonnelPerHospitalPatient       = ParameterInput.getInstance()
	            .getNumberOfPersonnelPerHospitalPatient();
	}
    private final World                    world;
    private final List<SubPopulation>      households;
    private final List<HospitalPopulation> hospitals;

    public PopulationFactory(World world) {
        this.world = world;
        households = new ArrayList<>(Input.numberOfHouseholds + Input.numberOfNursingHomes);
        hospitals = new ArrayList<>(Input.numberOfHospitals);
    }

    public List<HospitalPopulation> getHospitals() {
        return hospitals;
    }

    public List<SubPopulation> getHouseholds() {
        return households;
    }

    /**
     * Creates new households, hospitals, and nursing homes. Households and nursing homes are the main building blocks,
     * their inhabitants are here also created. Nursing homes have their personnel recruited here. Hospitals start with one
     * human assigned as personnel each, more personnel is recruited in HositalisationEvent.java (as number of patients so
     * needed personnel is not yet known here). Hospitals to go to as patient are assigned per household, but hospital personnel
     * is randomly reassigned a hospital to work at.
     */
    @Override
    public void initialize() {
        super.initialize();
        double x;
        int size = 0;

        LOGGER.info("creating {} households; this may take some time...", Input.numberOfHouseholds);
        for (int i = 0; i < Input.numberOfHouseholds; i++) {
            if (LOGGER.isDebugEnabled() && (i % 100 == 0)) {
                LOGGER.debug("creating household #{}", i);
            }
            final int assignedHospital = Input.numberOfHospitals > 0 ? i % Input.numberOfHospitals : 0;
            x = world.nextDouble();
            for (int j = 0; j < Input.hhSizeProbabilities.length; j++) {
                x -= Input.hhSizeProbabilities[j];
                if (x < 0) {
                    size = j + 1;
                    break;
                }
            }
            final HouseholdPopulation hh = new HouseholdPopulation(world, i);
            for (int j = 0; j < size; j++) {
                final Human h = new Human(world, hh, assignedHospital, false);
                hh.join(h);
            }
            households.add(hh);
            hh.startSubpopulation();
        }

        LOGGER.info("creating {} hospitals; ...", Input.numberOfHospitals);
        for (int i = 0; i < Input.numberOfHospitals; i++) {
            final HospitalPopulation hospital = new HospitalPopulation(world, i);
            hospitals.add(hospital);
            hospital.startSubpopulation();
            if (Input.numberOfPersonnelPerHospitalPatient > 0.) {
            	// hospitals each start with one member of personnel, additional personnel is recruited as needed, see 
            	// checkNumberOfPersonnelAndRecruitIfNeeded() within class HospitalisationEvent
                final AntibioticsHospitalTreeElement treeElement = (AntibioticsHospitalTreeElement) world.getGeneralTree()
                        .selectIRandom(HospitalStatesEnum.NOT_HOSPITALISED.index());
                treeElement.getHospitalProcessor().makeHospitalPersonnel(i);
            }
        }

        final int normalPopulationSize = world.tree("CC-0").size();
        LOGGER.info("creating {} nursing-homes; ...", Input.numberOfNursingHomes);
        for (int i = Input.numberOfHouseholds; i < Input.numberOfHouseholds + Input.numberOfNursingHomes; i++) {
            final int assignedHospital = Input.numberOfHospitals > 0 ? i % Input.numberOfHospitals : 0;
            x = world.nextDouble();
            for (int j = 0; j < Input.nhSizes.length; j++) {
                x -= Input.nhSizeProbabilities[j];
                if (x < 0) {
                    size = Input.nhSizes[j];
                    break;
                }
            }
            final NursingHomePopulation nh = new NursingHomePopulation(world, i);
            for (int j = 0; j < size; j++) {
                final Human h = new Human(world, nh, assignedHospital, true);
                nh.join(h);
            }
            households.add(nh);
            nh.startSubpopulation();
            final int numberOfPersonnel = (int) Math.ceil(nh.nUninfected("CC-0") * Input.numberOfPersonnelPerNursingHomeInhabitant);
            int j = 0;
            while (j < numberOfPersonnel) {
                final AntibioticsHospitalTreeElement treeElement = (AntibioticsHospitalTreeElement) world.getGeneralTree()
                        .selectIRandom(HospitalStatesEnum.NOT_HOSPITALISED.index());
                final Human human = treeElement.getHuman();
                if (human.getHumanNr() < normalPopulationSize && !human.isNursingHomePersonnel()) {
                    nh.join(human);
                    human.setExtraHousehold(nh);
                    human.setContactRateNHforPersonnel(world.getDistributions().drawFromContactratenhforpersonneldist());
                    human.setGiveExtraOutputExtraHousehold();
                    j++;
                }
            }

        }
    }
}

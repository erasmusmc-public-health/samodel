/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiCCsimSAbasics;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import inandoutput.OutputGenerator;
import inandoutput.ParameterInput;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;

/**
 * Main class to run the individual based multi-clonalcomplex Staphylococcus aureus simulation model.
 * @author roelb
 * @author Anneke S. de Vos
 */
public class SAsim {

    private static final Logger                            LOGGER = LoggerFactory.getLogger(World.class);
    // Parameter file to use in the model run.
    private static final String parameterFilename = "ModelParameters.yaml";

    public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {

        final long tm0 = System.nanoTime();

        // reading in the parameters from yaml through class ParameterInput
        final ParameterInput parameters = new ParameterInput().parameterRead(parameterFilename);
        LOGGER.info("Model parameters read from: " + parameterFilename);

        // simulation startTime within the model
        final long startTime = TimeUtilsDays.asTimePoint(0);
        // simulation endTime is calculated relative to startTime:
        final long stopTime = startTime + parameters.getSimDuration();

        for (int i = 0; i < parameters.getNumberOfRepeatRuns(); i++) {

            final long t0 = System.nanoTime();
            final World world = new World(startTime, stopTime, parameters, i);
            world.initialize();

            long t1 = System.nanoTime();
            LOGGER.info("world initialized with " + parameters.getNumberOfHouseholds() + " households, " +
                    parameters.getNumberOfNursingHomes() + " nursing homes and " + world.getHospitals().size() +
                    " hospitals (" + world.tree("CC-0").size() + " individuals) in " + (t1 - t0) / 1_000_000_000 + " seconds");
            world.start();

            t1 = System.nanoTime();
            if (parameters.getNumberOfRepeatRuns() > 1) {
            	LOGGER.info("simulation " + (i + 1) + " of " + parameters.getNumberOfRepeatRuns() + " took "
                        + (t1 - t0) / 1.0e9 + " seconds\n");
            } else {
            	LOGGER.info("simulation took " + (t1 - tm0) / 1.0e9 + " seconds");
            	LOGGER.info("Output folder: " + OutputGenerator.getFilePath());
            }
        }

        if (parameters.getNumberOfRepeatRuns() > 1) {
            final long tm1 = System.nanoTime();
            LOGGER.info(parameters.getNumberOfRepeatRuns() + " simulation runs took " + (tm1 - tm0) / 1.0e9 + " seconds");
            LOGGER.info("Output folder: " + OutputGenerator.getFilePath());
        }

    }
}

/**
 * Main building blocks (World, Household, Human) making up the individual based
 *  Staphylococcus Aureaus (SA) multi clonal-complex (CC) simulation model. 
 */
package multiCCsimSAbasics;
package multiCCsimSAbasics;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import antibiotics.AntibioticStatesEnum;
import hospitals.HospitalStatesEnum;
import inandoutput.ParameterInput;
import strains.InfectionStatesEnum;
import strains.StrainTreeElement;
import subPopulationGeneral.SubPopulation;
import subPopulationGeneral.SubPopulationTypeEnum;

/**
 * Class representing a human.
 *
 * @author roelb
 *
 */
public class Human {
	
    private static final Logger                  LOGGER = LoggerFactory.getLogger(World.class);
    private static class Input {
		private final static double                  hospitilisationHouseholdContactRateMultiplier       = ParameterInput
	            .getInstance().getHouseholdContactRateMultiplierForHospitalised();
	    private final static double                  nursinghomeContactratePopulationMultiplier          = ParameterInput
	            .getInstance().getNursinghomeContactratePopulationMultiplier();
	    private final static double                  nursinghomeExtraDailyAntibioticsUse                 = ParameterInput
	            .getInstance().getNursinghomeAntibioticsExtraDailyUsageAverage(); 
	    private final static double                  nursinghomeExtraDailyHospitalisation                = ParameterInput
	            .getInstance().getNursinghomeHospitalisationExtraDailyAverage();
	    private final static double                  daysUsingAntibiotics                      			 = ParameterInput
	            .getInstance().getAntibioticsTherapyLengthInDays();
	    private final static double                  daysHospitalised                            		 = ParameterInput
	            .getInstance().getHospitalStayDaysAverage();
	    private final static double                  fractionInRiskgroup                                 = ParameterInput
	            .getInstance().getFractionRiskgroup();
	    private final static double                  fractionOfHospitalContactsRemainingDespiteIsolation = ParameterInput
	            .getInstance().getFractionOfHospitalContactsRemainingDespiteIsolation();
    }
    private static long serialNumber = 0;
    // A reset of serial numbers is necessary in case of multiple runs,
	static long resetSerialNumber() {
    	return serialNumber = 0;
    }
    private InfectionStatesEnum                  overallInfState;
    private int                                  assignedHospital;                                                   // changed when set as hospital
                                                                                                                     // personnel
    private double                               contactrateHosp                                     = 0;            // set /reset at hospitalization
                                                                                                                     // events
    private int                                  hospitalWard                                        = -1;           // set /reset at hospitalization
    // events
    private SubPopulation                        extraHousehold                                      = null;         // set in case of nursing home
    // personnel
    private double                               contactRateNHforPersonnel                           = 0.;
    private boolean                              isGiveExtraOutputExtraHousehold                     = false;
    private boolean                              isAssumedMrsaPositive                               = false;
    private final boolean                        isIneFOIRiskgroup;
    private final double                         contactrateHHorNH;                                                  // may not be used, see
    // SubPopulation class options
    private final double                         contactratePop;
    private final long                           humanNr;
    private final double                         infectiousness;
    private final double                         susceptibility;
    private final double                         infectionDurationAverageInDays;

    private final double                         dailyAntibioticsUseRate;
    private final double                         dailyHospitalisationRate;
    private final World                          world;
    private final SubPopulation                  household;
    private final Map<String, StrainTreeElement> strains;
    
	private final AntibioticsHospitalTreeElement antibioticsAndHospitalState;
	private final boolean                        isGiveExtraOutput;
	public Human(World world, SubPopulation household, int assignedHospital, boolean isNursingHomeInhabitant) {
    	humanNr = getNextSerialNumber();
        this.world = world;
        this.household = household;
        this.assignedHospital = assignedHospital;
        isIneFOIRiskgroup = world.nextDouble() < Input.fractionInRiskgroup;
        overallInfState = InfectionStatesEnum.S;
        infectiousness = world.getDistributions().drawFromInfectiousnessDist();
        susceptibility = world.getDistributions().drawFromSusceptibilityDist();
        infectionDurationAverageInDays = world.getDistributions().drawFromInfectionDurationAverageDistribution();
        if (isNursingHomeInhabitant) {
            isGiveExtraOutput = getHousehold().getID() < ParameterInput.getInstance().getNumberOfHouseholds()
                    + ParameterInput.getInstance().getExtraOutputMaxNursingHomeID();
            contactrateHHorNH = world.getDistributions().drawFromContactratenhdist();
            contactratePop = world.getDistributions().drawFromContactratepopdist() * Input.nursinghomeContactratePopulationMultiplier;
            // Naively using the daily AB UseRate obtained from mean yearly usage, the actual yearly usage of AB would be less than aimed for, 
            // since in state TAKING_AB individuals cannot start AB. i.e. the RateActual would be RateNaive * (1 - RateActual * timeTakingAB).
        	// To make the RateActual equal to the RateNaive, we multiply it by the reverse factor, i.e. by 1 / (1 - RateNaive * timeTakingAB):
            double dailyAntibioticsUseRateActual = world.getDistributions().drawFromDailyABUseRateDist() +
            		Input.nursinghomeExtraDailyAntibioticsUse;
            dailyAntibioticsUseRate = dailyAntibioticsUseRateActual / (1 - dailyAntibioticsUseRateActual * Input.daysUsingAntibiotics);
            // See above for the AntibioticsUseRate, we similarly compensate for time in hospital not exposed to the hospitalization rate:
            double dailyHospitalisationRateActual = world.getDistributions().drawFromDailyHospilisationDist() + 
            		Input.nursinghomeExtraDailyHospitalisation;
            dailyHospitalisationRate = dailyHospitalisationRateActual / (1 - dailyHospitalisationRateActual * Input.daysHospitalised);
        } else {
            isGiveExtraOutput = getHousehold().getID() < ParameterInput.getInstance().getExtraOutputMaxHouseholdID();
            contactrateHHorNH = world.getDistributions().drawFromContactratehhdist();
            contactratePop = world.getDistributions().drawFromContactratepopdist();
            double dailyAntibioticsUseRateActual = world.getDistributions().drawFromDailyABUseRateDist();
            dailyAntibioticsUseRate = dailyAntibioticsUseRateActual / (1 - dailyAntibioticsUseRateActual * Input.daysUsingAntibiotics);
            double dailyHospitalisationRateActual = world.getDistributions().drawFromDailyHospilisationDist();
            dailyHospitalisationRate = dailyHospitalisationRateActual / (1 - dailyHospitalisationRateActual * Input.daysHospitalised);
        }
        strains = new HashMap<>();
        for (final String strainName : world.getStrainmodels().getModels().keySet()) {
            strains.put(strainName, new StrainTreeElement(this, world.getStrainmodels().getModels().get(strainName)));
        }
        antibioticsAndHospitalState = new AntibioticsHospitalTreeElement(this);
    }
	
    public AntibioticsHospitalTreeElement getAntibioticsAndHospitalState() {
        return antibioticsAndHospitalState;
    }
    
    public int getAssignedHospital() {
        return assignedHospital;
    }

    public double getContactratePop() {
        return contactratePop;
    }

    /**
     * For the sub-population infection rate calculation, depending on the sub-population type, the correct
     * contact rate is returned. This also enables the sub-population to calculate the infection-rate both
     * with and without taking into account possible hospitalization (and isolation) of its members.
     * @param whichContactrate Which subpopulation contact-rate. With BUT_HOSPITALISED: taking possible hospitalization
     * into account, otherwise the contact rate as if not hospitalized is returned.
     * @return The required sub-population contact-rate.
     */
    public double getContactrateSubPop(SubPopulationTypeEnum whichContactrate) {
        switch (whichContactrate) {
            case HOUSEHOLD:
                return contactrateHHorNH;
            case NURSINGHOME:
                return isNursingHomePersonnel() ? contactRateNHforPersonnel : contactrateHHorNH;
            case HOUSEHOLD_BUT_HOSPITALISED: // = taking possible hospitalization into account
                return contactrateHHorNH *
                        (antibioticsAndHospitalState.getHospitalState() == HospitalStatesEnum.HOSPITALISED
                                ? Input.hospitilisationHouseholdContactRateMultiplier
                                : 1);
            case NURSINGHOME_BUT_HOSPITALISED: // = taking possible hospitalization into account
                return (isNursingHomePersonnel() ? 0. : contactrateHHorNH) *
                        (antibioticsAndHospitalState.getHospitalState() == HospitalStatesEnum.HOSPITALISED
                                ? Input.hospitilisationHouseholdContactRateMultiplier
                                : 1.);
            case HOSPITAL:
                return contactrateHosp * (isAssumedMrsaPositive ? Input.fractionOfHospitalContactsRemainingDespiteIsolation : 1);
            default:
                LOGGER.error("error: use of undefined subpopulation");
                return 0.0;
        }
    }

    public double getDailyAntibioticsUseRate() {
        return dailyAntibioticsUseRate;
    }

    public double getDailyHospitalisationRate() {
        return dailyHospitalisationRate;
    }

    public SubPopulation getExtraHousehold() {
        return extraHousehold;
    }

    public int getHospitalWard() {
        return hospitalWard;
    }

    public SubPopulation getHousehold() {
        return household;
    }

    public double getInfectionDurationAverageInDays() {
        return infectionDurationAverageInDays;
    }

    public double getInfectiousness() {
        return infectiousness;
    }

    public long getHumanNr() {
        return humanNr;
    }

    private long getNextSerialNumber() {
    	return serialNumber++;
    }

    public InfectionStatesEnum getOverallInfState() {
        return overallInfState;
    }

    public Map<String, StrainTreeElement> getStrains() {
        return strains;
    }

    public double getSusceptibility() {
        return susceptibility;
    }

    public World getWorld() {
        return world;
    }

    public boolean isAssumedMrsaPositive() {
        return isAssumedMrsaPositive;
    }

    public boolean isGiveExtraOutput() {
        return isGiveExtraOutput;
    }

    public boolean isGiveExtraOutputExtraHousehold() {
        return isGiveExtraOutputExtraHousehold;
    }

    public boolean isIneFOIRiskgroup() {
        return isIneFOIRiskgroup;
    }

    public boolean isNursingHomePersonnel() {
        return !(extraHousehold == null);
    }

    public boolean isUsingAntibiotics() {
        return (antibioticsAndHospitalState.getAntibioticsState() == AntibioticStatesEnum.TAKING_AB);
    }

    public void setAssignedHospital(int assignedHospital) {
        this.assignedHospital = assignedHospital;
    }

    public void setAssumedMrsaPositive(boolean isTestedAsMrsaPositive) {
        isAssumedMrsaPositive = isTestedAsMrsaPositive;
    }

    public void setContactrateHosp(double contactrateHosp) {
        this.contactrateHosp = contactrateHosp;
    }

    public void setContactRateNHforPersonnel(double contactRateNHforPersonnel) {
        this.contactRateNHforPersonnel = contactRateNHforPersonnel;
    }

    public void setExtraHousehold(SubPopulation extraHousehold) {
        this.extraHousehold = extraHousehold;
    }

    /*
     * For nursing home personnel, extra output is given, if wanted at all, for those below a certain nursing home ID
     */
    public void setGiveExtraOutputExtraHousehold() {
        isGiveExtraOutputExtraHousehold = (getExtraHousehold().getID() < ParameterInput.getInstance().getNumberOfHouseholds()
                + ParameterInput.getInstance().getExtraOutputMaxNursingHomeID());
        if (isGiveExtraOutput) {
            household.SPoutput(",NHPers,", humanNr, "CC-0", ",ABs\n");
        }
        if (isGiveExtraOutputExtraHousehold) {
            extraHousehold.SPoutput(",NHPers,", humanNr, "CC-0", ",ABs\n");
        }
    }

    public void setHospitalWard(int hospWard) {
        hospitalWard = hospWard;
    }

    public void setOverallInfStateIMrsa() {
        overallInfState = InfectionStatesEnum.IMRSA;
    }

    public void setOverallInfStateISsa() {
        overallInfState = InfectionStatesEnum.ISSA;
    }

    public void setOverallInfStateS() {
        overallInfState = InfectionStatesEnum.S;
    }

    /**
     * Starts up a human, done by its household / nursing home after its creation.
     */
    public void startHuman() {
        for (final String strainName : strains.keySet()) {
            world.tree(strainName).insert(strains.get(strainName));
        }
        world.getGeneralTree().insert(antibioticsAndHospitalState);
    }

}

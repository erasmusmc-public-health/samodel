package multiCCsimSAbasics;

import antibiotics.AntibioticStatesEnum;
import inandoutput.ParameterInput;
import subPopulationGeneral.SubPopulation;
import subPopulationGeneral.SubPopulationTransmissionEvent;
import subPopulationGeneral.SubPopulationTypeEnum;

/**
 * Household class, deals with household transmission events.
 *
 */
public class HouseholdPopulation extends SubPopulation {

    public HouseholdPopulation(World world, int iD) {
        super(world, world.getDistributions().drawFromContactratehhdist(), iD);
        contactRateLevelOfVariation = SubPopulationContactRateLevelOfVariation.valueOf(
                ParameterInput.getInstance().getHouseholdContactRateLevelOfVariation());
        contactRateStructure = SubPopulationContactRateStructuring.valueOf(
                ParameterInput.getInstance().getHouseholdContactRateStructure().toUpperCase());
        alfa = ParameterInput.getInstance().getHouseholdContactRateSaturatingAlfa();
        whichContactRate = SubPopulationTypeEnum.HOUSEHOLD;
    }

    /**
     * The infection event may be canceled when members have fewer household-contacts due to hospitalization, or are
     * less infectious or susceptible due to antibiotics usage. Note, the last calculated rate must be the most accurate,
     * since in its calculation individual infection rates are stored, for drawing who gets infected.
     * @param e The transmission event.
     */
    @Override
    public void handle(SubPopulationTransmissionEvent e) {
        final double transRateIgnoringHospitilisationsAndAntibiotics = getSubPopulationTransmissionRate(
                e.getStrainmodel().getName(),
                SubPopulationTypeEnum.HOUSEHOLD, AntibioticStatesEnum.NOT_TAKING_AB);
        final double transRateWithHospitalisationsAndAntibiotics = getSubPopulationTransmissionRate(e.getStrainmodel().getName(),
                SubPopulationTypeEnum.HOUSEHOLD_BUT_HOSPITALISED, AntibioticStatesEnum.TAKING_AB);
        if (world.nextDouble() <= (transRateWithHospitalisationsAndAntibiotics
                / transRateIgnoringHospitilisationsAndAntibiotics)) {
            handle(e.getStrainmodel());
        } else {
            updateInfectionRate(e.getStrainmodel().getName());
        }
    }

}

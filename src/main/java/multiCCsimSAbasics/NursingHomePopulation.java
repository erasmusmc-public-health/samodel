package multiCCsimSAbasics;

import antibiotics.AntibioticStatesEnum;
import inandoutput.ParameterInput;
import subPopulationGeneral.SubPopulation;
import subPopulationGeneral.SubPopulationTransmissionEvent;
import subPopulationGeneral.SubPopulationTypeEnum;

/**
 * Nursing-home class, deals with nursing-home transmission events.
 * 
 */
public class NursingHomePopulation extends SubPopulation {

    public NursingHomePopulation(World world, int iD) {
    	super(world, world.getDistributions().drawFromContactratenhdist(), iD);
    	contactRateLevelOfVariation = SubPopulationContactRateLevelOfVariation.valueOf(
    			ParameterInput.getInstance().getNursingHomeContactRateLevelOfVariation());
    	contactRateStructure = SubPopulationContactRateStructuring.valueOf(
    			ParameterInput.getInstance().getNursingHomeContactRateStructure().toUpperCase());
    	alfa = ParameterInput.getInstance().getNursingHomeContactRateSaturatingAlfa();
    	whichContactRate = SubPopulationTypeEnum.NURSINGHOME;
     }
  
    /**
     * The infection event may be canceled when members have fewer household-contacts due to hospitalization, or are
     * less infectious or susceptible due to antibiotics usage. Note, the last calculated rate must be the most accurate,
     * since in its calculation individual infection rates are stored, for drawing who gets infected.     
     * @param e The transmission event.
     */
    @Override
    public void handle(SubPopulationTransmissionEvent e) {
		double transRateIgnoringHospitilisationsAndAntibiotics = getSubPopulationTransmissionRate(e.getStrainmodel().getName(),
				SubPopulationTypeEnum.NURSINGHOME, AntibioticStatesEnum.NOT_TAKING_AB);
		double transRateWithHospitalisationsAndAntibiotics = getSubPopulationTransmissionRate(e.getStrainmodel().getName(), 
				SubPopulationTypeEnum.NURSINGHOME_BUT_HOSPITALISED, AntibioticStatesEnum.TAKING_AB);
		if(world.nextDouble()<=(transRateWithHospitalisationsAndAntibiotics/transRateIgnoringHospitilisationsAndAntibiotics)) {
			handle(e.getStrainmodel());
		} else {
			updateInfectionRate(e.getStrainmodel().getName());
		}
    }
}
package multiCCsimSAbasics;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import antibiotics.AntibioticStatesEnum;
import antibiotics.AntibioticsStartEvent;
import hospitals.HospitalPopulation;
import hospitals.HospitalStatesEnum;
import hospitals.HospitalisationEvent;
import inandoutput.DistributionsKeeper;
import inandoutput.OutputGenerator;
import inandoutput.ParameterInput;
import nl.erasmusmc.mgz.eventmanager.EventManagerFactory;
import nl.erasmusmc.mgz.eventmanager.IEventManager;
import nl.erasmusmc.mgz.magicTree.IMagicTree;
import nl.erasmusmc.mgz.math.IMath;
import nl.erasmusmc.mgz.math.MathFactory;
import nl.erasmusmc.mgz.math.MathOptions;
import nl.erasmusmc.mgz.utils.process.impl.ProcessManager;
import nl.erasmusmc.mgz.utils.process.impl.StartableProcess;
import nl.erasmusmc.mgz.utils.process.model.IInitializableProcess;
import nl.erasmusmc.mgz.utils.process.model.IStartableProcess;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import strains.ExternalTransmissionEvent;
import strains.HowEnum;
import strains.InfectionStatesEnum;
import strains.PopulationTransmissionEvent;
import strains.StrainModels;
import strains.StrainStatesEnum;
import strains.StrainTreeElement;
import subPopulationGeneral.SubPopulation;
import util.mtree.ElementMagicTree;
import util.mtree.IElementMagicTree;
import util.mtree.ITreeElement;

/**
 * World class, brings together all components of the model.
 *
 * World lacks XmlInput, ObserverRegister, PopulationRegister, HumanFactory, OutputManager and ProgressIndicator, so
 * inheriting from CoreWorld would bring a lot of unneeded clutter. So we don't extend CoreWorld.
 *
 * @author roelb
 * @author rinke
 *
 */
public class World extends StartableProcess implements IStartableProcess {

    private static final Logger                            LOGGER = LoggerFactory.getLogger(World.class);
    private final ProcessManager                           processManager;
    private final OutputGenerator                          outputGenerator;
    private final ParameterInput                           parameters;
    private final DistributionsKeeper                      distributions;
    private final long                                     repeat;
    private final long                                     stopTime;
    private final double                                   startTimeAsDay;
    private final double                                   stopTimeAsDay;
    private final IMath                                    math;
    private final IEventManager                            eventManager;
    private final Map<String, IElementMagicTree>           trees;
    private final PopulationTransmissionEvent[]	 		   populationTransmissionEvents;
    private final ExternalTransmissionEvent[]		       externalTransmissionEvents;
    private final IElementMagicTree                        generalTree;
    private final AntibioticsStartEvent                    antibioticsStartEvent;
    private final HospitalisationEvent                     hospitalisationEvent;
    private final StrainModels                             strainmodels;
    private final PopulationFactory                        populationFactory;

    public World(long startTime, long stopTime, ParameterInput parameterInput, long repeat) throws IOException {
        parameters = parameterInput;
        math = assignMath();
        distributions = new DistributionsKeeper(this);
        this.repeat = repeat;
        this.stopTime = stopTime;
        startTimeAsDay = TimeUtilsDays.timePointAsDays(startTime);
        stopTimeAsDay = TimeUtilsDays.timePointAsDays(stopTime) - startTimeAsDay;
        eventManager = EventManagerFactory.newFactory().getIEventManagerInstance();
        eventManager.setRunTimes(startTime, stopTime);
        LOGGER.info("start: {}", TimeUtilsDays.timePointAsString(startTime));
        populationFactory = new PopulationFactory(this);
        strainmodels = new StrainModels(math);
        trees = new HashMap<>();
        populationTransmissionEvents = new PopulationTransmissionEvent[parameters.getNumberOfStrains()]; 
        externalTransmissionEvents = new ExternalTransmissionEvent[parameters.getNumberOfStrains()];
        // 2 states (S and I), and 3 rates (individual internal population susceptibility and infectiousness, and susceptibility from external
        // sources)
        final int numberOfStrainStates = StrainStatesEnum.values().length;
        final int numberOfStrainRates = StrainStatesEnum.values().length + 1;
        int i = 0;
        for (final String strainName : strainmodels.getModels().keySet()) {
            trees.put(strainName, new ElementMagicTree(numberOfStrainRates, numberOfStrainStates, math.getRandomGenerator()));
            populationTransmissionEvents[i] = new PopulationTransmissionEvent(this, strainName);
            externalTransmissionEvents[i] = new ExternalTransmissionEvent(this, strainName);
            i++;
        }
        // max 3 hospital states + max 3 antibiotics states. 2 rates: hospitalization and starting AB
        final int numberOfStates = HospitalStatesEnum.numberOfHospitalStatesUsed()
                + AntibioticStatesEnum.numberOfAntibioticStatesUsed();
        final int numberOfRates = 2;
        generalTree = new ElementMagicTree(numberOfRates, numberOfStates, math.getRandomGenerator());
        antibioticsStartEvent = new AntibioticsStartEvent(this);
        hospitalisationEvent = new HospitalisationEvent(this);
        outputGenerator = new OutputGenerator(this);
        processManager = new ProcessManager();
        processManager.register(new IInitializableProcess[] {
            eventManager,
            populationFactory,
            outputGenerator
        });
    }

    /**
     * Assigns the math module. This method is copied from CoreWorld.
     */
    protected IMath assignMath() {
        if (State.IDLE != getState()) {
            final String msg = "Trying to assign math to a world that has already been initialized.";
            LOGGER.error(msg);
            throw new IllegalStateException(msg);
        }
        return MathFactory.newFactory().getIMathInstance(MathOptions.builder().seed(parameters.getSeed() + repeat).build());
    }

    public DistributionsKeeper getDistributions() {
        return distributions;
    }

    public final IEventManager getEventManager() {
        return eventManager;
    }

    public final IElementMagicTree getGeneralTree() {
        return generalTree;
    }

    public final List<HospitalPopulation> getHospitals() {
        return populationFactory.getHospitals();
    }

    public final List<SubPopulation> getHouseholds() {
        return populationFactory.getHouseholds();
    }

    public final IMath getMath() {
        return math;
    }

    public final ParameterInput getParameters() {
        return parameters;
    }

    public final long getRepeat() {
        return repeat;
    }

    public final double getStartTimeAsDay() {
        return startTimeAsDay;
    }

    public final long getStopTime() {
        return stopTime;
    }

    public final double getStopTimeAsDay() {
        return stopTimeAsDay;
    }

    public final StrainModels getStrainmodels() {
        return strainmodels;
    }

    public final Map<String, IElementMagicTree> getTrees() {
        return trees;
    }

    @Override
    public void initialize() {
        super.initialize();
        Human.resetSerialNumber();
        processManager.initialize();
    }

    public double nextDouble() {
        return math.nextDouble();
    }

    public double nextExp(double mean) {
        return math.nextExp(mean);
    }

    @Override
    public void stop() {
        eventManager.stop();
    }

    public IElementMagicTree tree(String strain) {
        return trees.get(strain);
    }
    
    @Override
    /**
     * The model is started by set-up of the magic-trees for each SA CC strain, creation of households,
     * and the setting up of initial infections, after which the self rescheduling events of antibiotics,
     * hospitalisation, and transmission are started, and finally the event manager takes off.
     */
    public void start() {
        super.start();
        for (final String strainName : strainmodels.getModels().keySet()) {
            final long t0 = System.nanoTime();
            final IMagicTree<ITreeElement> tree = trees.get(strainName);
            final int nrOfInfectionsToSeed = (int) Math.round(
            		tree.size() * strainmodels.getModels().get(strainName).getFractionInitiallyInfected()
            		);
            int nrInfected = 0;
            while (nrInfected < nrOfInfectionsToSeed) {
                final StrainTreeElement strain = (StrainTreeElement) tree.selectDRandom(StrainStatesEnum.S.ordinal());
                // humans already infected by another strain cannot be infected, they should therefore not count towards
                // obtaining the wanted nrOfInfectionsToSeed, hence the following added check before nrInfected++
                if (strain.getHuman().getOverallInfState() == InfectionStatesEnum.S) {
                    strain.infect(HowEnum.START);
                    nrInfected++;
                }
            }
            final long t1 = System.nanoTime();
            LOGGER.info(nrOfInfectionsToSeed + " " + strainName + " infections seeded in " + (t1 - t0) / 1_000_000 + "ms");
        }
        antibioticsStartEvent.execute();
        if (ParameterInput.getInstance().getNumberOfHospitals() > 0) {
            hospitalisationEvent.execute();
        }
        for (int i = 0; i < parameters.getNumberOfStrains(); i++) {
            populationTransmissionEvents[i].execute();
            externalTransmissionEvents[i].execute();
        }
        eventManager.start();
    }

}

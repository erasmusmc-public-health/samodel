package multiCCsimSAbasics;
import antibiotics.AntibioticStatesEnum;
import antibiotics.AntibioticsTreeElementProcessor;
import hospitals.HospitalStatesEnum;
import hospitals.HospitalTreeElementProcessor;
import inandoutput.ParameterInput;
import util.mtree.ITreeElement;
import util.mtree.TreeElement;

/**
 * Elements of the general tree. Hold antibiotics (AB) and hospital related status. For greater clarity the functions relating
 * to antibiotics and hospital change are held in separate processors, only magic tree specifics are handled here. 
 * Tree rate 0 represents the rate of hospitalization of individuals, rate 1 their rate of starting antibiotics treatment. 
 * Indexes (flags) are flexibly assigned, depending on the number of included hospital and AB states (i.e. on parameter input, see also 
 * AntibioticStatesEnum and HospitalStatesEnum). The first index, 0, represents status as not hospitalized, the next represents hospital 
 * personnel (but only if personnel is modeled), and then comes status as hospitalized patient. The next flag represents not taking 
 * antibiotics currently, then, if ab usage is not instantaneous, taking antibiotics, and, if used, the final tree index represents having
 *  recently used antibiotics. 
 * 
 * @author Anneke S. de Vos
 *
 */
public class AntibioticsHospitalTreeElement extends TreeElement implements ITreeElement {

    public final static int	 	 treeIndexDailyHospitalisationRate = 0; // first tree rate = rate of hospitalization
    public final static int      treeIndexDailyABrate = 1; //second tree rate = rate of starting AB
	private static class Input {
	    private static final double	 				dailyHospitalABrate = ParameterInput.getInstance().getAntibioticsHospitalExtraDailyUsageAverage();
	}
	
    private AntibioticStatesEnum abState;
    private HospitalStatesEnum   hospitalState;
    private boolean				 targetedTreatmentIsOn = false;
    private final Human      	 human;
    private final double	 	 dailyHospitalisationRate;
    private final double    	 dailyABrate;
    private final AntibioticsTreeElementProcessor antibioticsProcessor;
    private final HospitalTreeElementProcessor hospitalProcessor;
    		
    public AntibioticsHospitalTreeElement(Human human) {
    	super(human.getHumanNr(), HospitalStatesEnum.numberOfHospitalStatesUsed() + AntibioticStatesEnum.numberOfAntibioticStatesUsed(), 2); 
    				// max 3 hospital states + max 3 antibiotics states. 2 rates: hospitalization and starting AB, see above
        this.human = human;
        this.dailyHospitalisationRate = human.getDailyHospitalisationRate();
        this.dailyABrate = human.getDailyAntibioticsUseRate();
        this.hospitalState = HospitalStatesEnum.NOT_HOSPITALISED;
        setFlag(hospitalState.index());
        setRate(dailyHospitalisationRate, treeIndexDailyHospitalisationRate);
        this.abState = AntibioticStatesEnum.NOT_TAKING_AB;
        setFlag(abState.index());
        setRate(dailyABrate, treeIndexDailyABrate);
        this.antibioticsProcessor = new AntibioticsTreeElementProcessor(this);
        this.hospitalProcessor = new HospitalTreeElementProcessor(this);
    }
    
    public AntibioticStatesEnum getAntibioticsState() {
        return abState;
    }

	public void setAntibioticsState(AntibioticStatesEnum abState) {
		this.abState = abState;
	}

	public boolean isTargetedTreatmentIsOn() {
		return targetedTreatmentIsOn;
	}

	public void setTargetedTreatmentIsOn(boolean targetedTreatmentIsOn) {
		this.targetedTreatmentIsOn = targetedTreatmentIsOn;
	}

	public void setHospitalState(HospitalStatesEnum hospitalState) {
		this.hospitalState = hospitalState;
	}
	
    public HospitalStatesEnum getHospitalState() {
        return hospitalState;
    }
    
    public Human getHuman() {
        return human;
    }
    
    public AntibioticsTreeElementProcessor getAntibioticsProcessor() {
		return antibioticsProcessor;
	}

	public HospitalTreeElementProcessor getHospitalProcessor() {
		return hospitalProcessor;
	}
	
    /**
     * When an AB treatment starts or ends, the tree nodes (AB status and AB start rate) are updated.
     */
    public void updateFlagsRatesTreeAB() {
    	for (int i = HospitalStatesEnum.numberOfHospitalStatesUsed();
    			i < HospitalStatesEnum.numberOfHospitalStatesUsed() + AntibioticStatesEnum.numberOfAntibioticStatesUsed(); i++) {
    		clearFlag(i);
    	}
        setFlag(abState.index());
        if (abState != AntibioticStatesEnum.TAKING_AB) {
            if (hospitalState == HospitalStatesEnum.HOSPITALISED) {
                setRate(dailyABrate  + Input.dailyHospitalABrate, treeIndexDailyABrate);
            } else {
            	setRate(dailyABrate, treeIndexDailyABrate);
            }
        } else {
            setRate(0.0, treeIndexDailyABrate);
        }
        human.getWorld().getGeneralTree().update(this);
    }
	
    /**
     * When hospitalization starts or ends, the tree nodes (hospital status, hospitalization 
     * rate, as well AB start rate) are updated.
     */
    public void updateFlagsRatesTreeHospital() {
    	for (int i = 0; i < HospitalStatesEnum.numberOfHospitalStatesUsed(); i++) {
    		clearFlag(i);
    	}
        setFlag(hospitalState.index());
        if (hospitalState == HospitalStatesEnum.NOT_HOSPITALISED) {
            setRate(dailyHospitalisationRate, treeIndexDailyHospitalisationRate);
        } else {
        	setRate(0, treeIndexDailyHospitalisationRate);
        }
        if (abState != AntibioticStatesEnum.TAKING_AB) {
            if (hospitalState == HospitalStatesEnum.HOSPITALISED) {
                setRate(dailyABrate + Input.dailyHospitalABrate, treeIndexDailyABrate);
            } else {
            	setRate(dailyABrate, treeIndexDailyABrate);
            }
        }
        human.getWorld().getGeneralTree().update(this);
    }
  
}

package antibiotics;

import hospitals.HospitalStatesEnum;
import inandoutput.ParameterInput;
import multiCCsimSAbasics.AntibioticsHospitalTreeElement;
import multiCCsimSAbasics.Human;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import strains.InfectionStatesEnum;
import strains.StrainStatesEnum;
import strains.StrainTreeElement;

/**
 * Holds antibiotics (AB) processes related functions. Magic tree specifics must be handled in the AntibioticsHospitalTreeElement itself.
 * At the end of AB therapy, SA may be cleared. AB may be instantaneous or last a duration, during which an individuals susceptibility 
 * and infectiousness are lowered. There may be an additional period after taking AB during which susceptibility is raised (due to less
 * competition from other bacteria). These effects are implemented through the Strain classes, eventually by the Household class. 
 *  
 * @author Anneke S. de Vos
 *
 */
public class AntibioticsTreeElementProcessor {
	
	private static class Input {
	    private static final long       			treatmentDuration = TimeUtilsDays.asDuration(ParameterInput.getInstance().
	    												getAntibioticsTherapyLengthInDays());
	    private static final boolean    			treatmentIsNotInstantaneous = !(treatmentDuration == 0.0);
	    private static final long       			extraSusceptibleDuration = TimeUtilsDays.asDuration(ParameterInput.getInstance().
	    												getAfterAntibioticsAlteredSusceptibilityPeriodInDays()); 
	    private static final boolean    			susceptibilityIsRaisedAfterTreatment = !(extraSusceptibleDuration == 0.0);
	}
	
	private final AntibioticsHospitalTreeElement treeElement;
    private final Human      	 human;
    private AntibioticsAfterPeriodEndEvent 		endExtraSusceptiblePeriodEvent = null; // need this as field, as this event may be cancelled if new AB starts
    
	public AntibioticsTreeElementProcessor(AntibioticsHospitalTreeElement treeElement) {
		this.treeElement = treeElement;
		this.human = treeElement.getHuman();
	}
	
	/* 
     * someone can start AB if they are in the more susceptible recently_took_AB state. Then if treatment is not
     * instantaneous, the extra susceptibility is stopped and taking AB starts. If treatment is instantaneous,
     *  then the end of their more susceptible period is only extended (after an AB clear happensgetAntibioticsState())
     */
    public void startAB() {
        if (treeElement.getAntibioticsState() == AntibioticStatesEnum.RECENTLY_TOOK_AB) {
            this.endExtraSusceptiblePeriodEvent.cancel();
            if (Input.treatmentIsNotInstantaneous) {
            	endRecentlyTookABEffectsOnSusceptibility();
                takingABEffects();
            } else {
        		clearByAB();
                scheduleEndExtraSusceptiblePeriodEvent();
            }
        } else { // so AB starts without recent AB effects
        	if (Input.treatmentIsNotInstantaneous) {
        		takingABEffects();
        	} else {
        		clearByAB();
        		if (Input.susceptibilityIsRaisedAfterTreatment) {
        			extraSusceptiblePeriodEffects();
        		}
        	}
        }
    }
    
    public void endAB() {
        clearByAB();
        if (Input.susceptibilityIsRaisedAfterTreatment) {
            extraSusceptiblePeriodEffects();
        } else {
            endABeffectsOnSusceptibilityAndInfectvity();
        }
    }
    
    public void endExtraSusceptiblePeriod() {
        endRecentlyTookABEffectsOnSusceptibility();
    }
    
    private void takingABEffects() {
        startABeffectsOnSusceptibilityAndInfectvity();
        scheduleABEndEvent(); 
    }
    
    /*
     * Effects of AB taking are directly put into effect on population level, on household or hospital level,
     * already scheduled infection events may be cancelled based on recalculation at the scheduled event. 
     */
    private void startABeffectsOnSusceptibilityAndInfectvity() {
    	treeElement.setAntibioticsState(AntibioticStatesEnum.TAKING_AB);
    	treeElement.updateFlagsRatesTreeAB();
        for (final StrainTreeElement strain : human.getStrains().values()) {
       		strain.startstopABeffects();
        }
        if (human.isGiveExtraOutput()) {updateHouseholdWithExtraOutputInfo(",startAB,");}
        if (human.isGiveExtraOutputExtraHousehold()) {updateExtraHouseholdWithExtraOutputInfo(",startAB,");}
    }
    
    /* *
     * Checks if someone is infected with an SA CC, if so, calls to that strain for a possible cure effect
     * if someone was receiving targeted treatment, this is turned off for the next treatment. 
     */
    private void clearByAB() {
        if (human.getOverallInfState() != InfectionStatesEnum.S) {
            final String strain = infectedByWhichStrain();
            human.getStrains().get(strain).antibioticsPossibleCure(treeElement.isTargetedTreatmentIsOn());
        } 
        treeElement.setTargetedTreatmentIsOn(false);
    }
    
    private void endABeffectsOnSusceptibilityAndInfectvity() {
    	treeElement.setAntibioticsState(AntibioticStatesEnum.NOT_TAKING_AB);
    	treeElement.updateFlagsRatesTreeAB();
        for (final StrainTreeElement strain : human.getStrains().values()) {
      		strain.startstopABeffects();
        }
        if (human.isGiveExtraOutput()) {updateHouseholdWithExtraOutputInfo(",stopAB,");}
        if (human.isGiveExtraOutputExtraHousehold()) {updateExtraHouseholdWithExtraOutputInfo(",stopAB,");}
    }
    
    private void endRecentlyTookABEffectsOnSusceptibility() {
    	treeElement.setAntibioticsState(AntibioticStatesEnum.NOT_TAKING_AB);
    	treeElement.updateFlagsRatesTreeAB();
        for (final StrainTreeElement strain : human.getStrains().values()) {
       		strain.stopAfterABeffects();
        }
        updateSubpopulationInfectionrates();
        if (human.isGiveExtraOutput()) {updateHouseholdWithExtraOutputInfo(",endESP,");} 
        if (human.isGiveExtraOutputExtraHousehold()) {updateExtraHouseholdWithExtraOutputInfo(",endESP,");}
    }
    
    private void extraSusceptiblePeriodEffects() {
        recentlyTookABEffectsOnSusceptibility();
        scheduleEndExtraSusceptiblePeriodEvent();
    }
    
    /**
	    * Assumes only single strain infection to be possible
	    * @return The SA CC that someone is infected with if infected, otherwise null 
	    */
	private String infectedByWhichStrain() {
	        for (final String strainName : human.getStrains().keySet()) {
	            if (human.getStrains().get(strainName).getState() == StrainStatesEnum.I) {
	                return strainName;
	            }
	        }
	        return null;
	}
    
    /*
     * After AB therapy susceptibility may be raised, since the probability of infection is now increased the 
     * household or hospital infection rate must be updated, which is costly. 
     */
    private void recentlyTookABEffectsOnSusceptibility() {
    	treeElement.setAntibioticsState(AntibioticStatesEnum.RECENTLY_TOOK_AB);
    	treeElement.updateFlagsRatesTreeAB();
        for (final StrainTreeElement strain : human.getStrains().values()) {
       		strain.stopABstartAfterABeffects();
        }
        updateSubpopulationInfectionrates();
        if (human.isGiveExtraOutput()) {updateHouseholdWithExtraOutputInfo(",stopABsESP,");} 
        if (human.isGiveExtraOutputExtraHousehold()) {updateExtraHouseholdWithExtraOutputInfo(",stopABsESP,");}
    }

    private void scheduleABEndEvent() {
        AntibioticsEndEvent endABEvent = new AntibioticsEndEvent(this, human.getWorld().getEventManager());
        endABEvent.scheduleRelative(Input.treatmentDuration); 
    }
    
    private void scheduleEndExtraSusceptiblePeriodEvent() {
	   this.endExtraSusceptiblePeriodEvent = new AntibioticsAfterPeriodEndEvent(this, human.getWorld().getEventManager());
	   this.endExtraSusceptiblePeriodEvent.scheduleRelative(Input.extraSusceptibleDuration);
    }
    
    /*
     * Needed for the after antibiotics raised susceptibility period, hospitals get updated for entry and 
     * leaving of patients in HospitalisationEvent.java
     */
    private void updateHospitalInfectionrates() {
        for (final StrainTreeElement strain : human.getStrains().values()) {
        	human.getWorld().getHospitals().get(human.getAssignedHospital()).updateInfectionRate(strain);
        }
    }
    
    /*
     * @param newstate What is the new antibiotic state. For extra output purposes.
     */
    private void updateHouseholdWithExtraOutputInfo(String newstate) {
       		human.getHousehold().SPoutput(newstate, human.getHumanNr(), "CC-0", ",ABs\n");
    }
    
    private void updateExtraHouseholdWithExtraOutputInfo(String newstate) {
   		human.getExtraHousehold().SPoutput(newstate, human.getHumanNr(), "CC-0", ",ABs\n");
    }
    
    /**
     * When after antibiotics usage susceptibility is raised, household and hospital infection rates
     * must be updated so the time to the next infection event here is recalculated.
     */
    private void updateSubpopulationInfectionrates() {
        for (final StrainTreeElement strain : human.getStrains().values()) {
       		human.getHousehold().updateInfectionRate(strain);
            if (human.isNursingHomePersonnel()) {human.getExtraHousehold().updateInfectionRate(strain);}
        }
        if (treeElement.getHospitalState() != HospitalStatesEnum.NOT_HOSPITALISED) {updateHospitalInfectionrates();}
    }

}

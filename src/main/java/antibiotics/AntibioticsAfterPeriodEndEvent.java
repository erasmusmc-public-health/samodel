package antibiotics;

import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.eventmanager.IEventManager;


/**
 * Event that can be scheduled in the eventmanager, to end the period after antibiotics usage where an 
 * individual has higher susceptiblity to SA (due to lack of competition by other bacteria).
 * @author Anneke S. de Vos
 *
 */
public class AntibioticsAfterPeriodEndEvent extends Event implements IEvent {
	    
	private final AntibioticsTreeElementProcessor antibiotics;
	 
	    public AntibioticsAfterPeriodEndEvent(AntibioticsTreeElementProcessor antibiotics, IEventManager eventManager) {
	        super(eventManager);
	        this.antibiotics = antibiotics;
	    }

	    @Override
	    public void execute() {
	        antibiotics.endExtraSusceptiblePeriod();
	    }

}

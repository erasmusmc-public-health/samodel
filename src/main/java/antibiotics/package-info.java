/**
 * Classes managing use and effects of antibiotics. 
 * However, AntibioticsAndHospitalTreeElements contains the tree and so antibiotic status.
 */
package antibiotics;
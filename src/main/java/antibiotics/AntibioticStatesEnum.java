package antibiotics;

import hospitals.HospitalStatesEnum;
import inandoutput.ParameterInput;

/**
 * Antibiotics may be instantaneous or last a duration, during which an individuals
 * AntibioticsState is set to TAKING_AB. There may be an additional period after
 * taking antibiotics during which susceptibility is raised, i.e. RECENTLY_TOOK_AB.
 * So that we do not create an unnecessarily big tree (to be memory efficient), a
 * static numberOfAntibioticStatesUsed(), and an adaptable tree index, both depending
 * on actually used states, are added. The tree index here keeps count from
 * HospitalStatesEnum index max, as both states are kept track of in the same tree.
 * @author Anneke S. de Vos
 */
public enum AntibioticStatesEnum {
	
    NOT_TAKING_AB(0 + HospitalStatesEnum.numberOfHospitalStatesUsed()),
    
    TAKING_AB(1 + HospitalStatesEnum.numberOfHospitalStatesUsed()),
    
    RECENTLY_TOOK_AB(1 + (ParameterInput.getInstance().getAntibioticsTherapyLengthInDays() == 0.0 ? 0 : 1) +
                            HospitalStatesEnum.numberOfHospitalStatesUsed()); // if AB use is instantaneous, TAKING_AB state
    							// is unused, then RECENTLY_TOOK_AB (if this state is used) gets the next index in the tree

    private final static boolean treatmentIsNotInstantaneous          = !(ParameterInput.getInstance()
            .getAntibioticsTherapyLengthInDays() == 0.0);

    private final static boolean susceptibilityIsRaisedAfterTreatment = !(ParameterInput.getInstance()
            .getAfterAntibioticsAlteredSusceptibilityPeriodInDays() == 0.0);

    public final static int numberOfAntibioticStatesUsed() {
        return (1 + (treatmentIsNotInstantaneous ? 1 : 0) + (susceptibilityIsRaisedAfterTreatment ? 1 : 0));
    }

    private final int index;

    private AntibioticStatesEnum(int index) {
        this.index = index;
    }

    public int index() {
        return index;
    }

}

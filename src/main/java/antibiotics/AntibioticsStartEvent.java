package antibiotics;

import multiCCsimSAbasics.AntibioticsHospitalTreeElement;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import subPopulationGeneral.SelfRepeatingEvent;
import util.mtree.IElementMagicTree;

/**
 * Event that can be scheduled in the eventmanager, starts antibiotics usage by one person in the population. 
 * Each AntibioticsStartEvent also schedules the next event of this type, based on current use rates.
 * @author Anneke S. de Vos
 */
public class AntibioticsStartEvent extends SelfRepeatingEvent implements IEvent {

    private final IElementMagicTree tree;
    private final int 				treeIndex  = AntibioticsHospitalTreeElement.treeIndexDailyABrate;
    
    public AntibioticsStartEvent(World world) {
        super(world);
        this.tree = world.getGeneralTree();
    }

    @Override
    public void eventToSchedule() {
        final AntibioticsHospitalTreeElement treeElement = (AntibioticsHospitalTreeElement) tree.selectDRandom(treeIndex);
        treeElement.getAntibioticsProcessor().startAB();
    }
    
    @Override
    public double sumRates() {
    	return tree.sum(treeIndex);
    }
    
}

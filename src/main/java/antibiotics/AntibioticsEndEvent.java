package antibiotics;

import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.eventmanager.IEventManager;

/**
 * Event that can be scheduled in the eventmanager, to end antibiotics therapy.
 * @author Anneke S. de Vos
 */
public class AntibioticsEndEvent extends Event implements IEvent {
	    
	private final AntibioticsTreeElementProcessor antibiotics;
	 
	    public AntibioticsEndEvent(AntibioticsTreeElementProcessor antibiotics, IEventManager eventManager) {
	        super(eventManager);
	        this.antibiotics = antibiotics;
	    }

	    @Override
	    public void execute() {
	        antibiotics.endAB();
	    }

}

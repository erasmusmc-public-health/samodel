/*

This work is licensed under the Creative Commons Attribution-NonCommercial-
NoDerivatives 4.0 International License. To view a copy of this license,
visit http://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

2013 - 2019; original design and source code by Roel Bakker @ Skardahl / ROEL BAKKER Holding BV
2019 - 2020; refactored by Roel Bakker @
the department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
email: roel.bakker@gmail.com

 */

package util.mtree;

import java.util.Arrays;
import java.util.BitSet;

/**
 *
 * @author roelb
 * @author rinke (adaptations to sim-commons)
 */
public class TreeElement implements ITreeElement {

    long                   key;
    private final BitSet   flags;
    private final double[] rates;
    private double         contactrate; // independent of state

    public TreeElement(long key, int nflags, int ndoubles) {
        this.key = key;
        flags = new BitSet(nflags);
        rates = new double[ndoubles];
    }

    public TreeElement(long key, BitSet flags, double[] rates) {
        this.key = key;
        this.flags = (BitSet) flags.clone();
        this.rates = Arrays.copyOf(rates, rates.length);
    }

    @Override
    public TreeElement clone() {
        return new TreeElement(key, flags, rates);
    }

    @Override
    public int compareTo(ITreeElement other) {
        return Long.compare(key, other.key());
    }

    @Override
    public void setContactRate(double contactrate) {
        this.contactrate = contactrate;
    }

    @Override
    public double[] getRates() {
        return Arrays.copyOf(rates, rates.length);
    }

    @Override
    public double getRate(int index) {
        return rates[index];
    }

    @Override
    public BitSet getFlags() {
        return (BitSet) flags.clone();
    }

    @Override
    public boolean getFlag(int bitIndex) {
        return flags.get(bitIndex);
    }

    @Override
    public void setFlag(int bitIndex) {
        flags.set(bitIndex);
    }

    @Override
    public void clearFlag(int bitIndex) {
        flags.clear(bitIndex);
    }

    @Override
    public long key() {
        return key;
    }

    @Override
    public void setRate(double x, int index) {
        rates[index] = x;
    }

    @Override
    public void setRateExclusive(double x, int index) {
        for (int i = 0; i < rates.length; i++)
            if (i != index)
                rates[i] = 0.;
            else
                rates[i] = x;
    }

    @Override
    public void setFlagExclusive(int bitIndex) {
        flags.clear();
        flags.set(bitIndex);
    }

    @Override
    public double getContactRate() {
        return contactrate;
    }

}

/*

This work is licensed under the Creative Commons Attribution-NonCommercial-
NoDerivatives 4.0 International License. To view a copy of this license,
visit http://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

2013 - 2019; original design and source code by Roel Bakker @ Skardahl / ROEL BAKKER Holding BV
2019 - 2020; refactored by Roel Bakker @
the department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
email: roel.bakker@gmail.com

@see     <a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.680.7561&rep=rep1&type=pdf">
                R. Bakker et al. Rapid Weighted Random Selection in Agent-based Models of Infectious Disease Dynamics Using Augmented B-trees.
               SIMUL 2013 : The Fifth International Conference on Advances in System Simulation</a>


 */
package util.mtree;

import java.util.BitSet;
import java.util.Iterator;

import org.apache.commons.math3.random.RandomGenerator;

import nl.erasmusmc.mgz.magicTree.IMagicTree;

/**
 * ElementMagicTree is a shell around a sim-commons MagicTree.
 * @author rinke
 */
public class ElementMagicTree implements IElementMagicTree {

    private final IMagicTree<ITreeElement> tree;
    private double                         totalContactRate = 0.;

    public ElementMagicTree(int nDoubles, int nInts, RandomGenerator rng) {
        tree = IMagicTree.getInstance3(nDoubles, nInts, rng);
    }

    @Override
    public double totalContactRate() {
        return totalContactRate;
    }

    @Override
    public boolean insert(ITreeElement e) {
        totalContactRate += e.getContactRate();
        return tree.insert(e, e.getRates(), e.getFlags());
    }

    @Override
    public boolean clearBit(ITreeElement key, int bit) {
        return tree.clearBit(key, bit);
    }

    @Override
    public boolean containsKey(ITreeElement query) {
        return tree.containsKey(query);
    }

    @Override
    public int countFlag(int index) {
        return tree.countFlag(index);
    }

    @Override
    public int countFlag(ITreeElement from, ITreeElement to, int index) {
        return tree.countFlag(from, to, index);
    }

    @Override
    public int[] countFlags(int[] indexes) {
        return tree.countFlags(indexes);
    }

    @Override
    public int[] countFlags(ITreeElement from, ITreeElement to, int[] indexes) {
        return tree.countFlags(from, to, indexes);
    }

    @Override
    public void delete(ITreeElement key) {
        totalContactRate -= key.getContactRate();
        tree.delete(key);
    }

    @Override
    public ITreeElement findKey(ITreeElement query) {
        return tree.findKey(query);
    }

    @Override
    public boolean getBit(ITreeElement key, int index) {
        return tree.getBit(key, index);
    }

    @Override
    public BitSet getBitSet(ITreeElement key) {
        return tree.getBitSet(key);
    }

    @Override
    public double getValue(ITreeElement key, int index) {
        return tree.getValue(key, index);
    }

    @Override
    public boolean insert(ITreeElement key, BitSet flags) {
        throw new UnsupportedOperationException("use version with 1 single param");
    }

    @Override
    public boolean insert(ITreeElement key, double[] values) {
        throw new UnsupportedOperationException("use version with 1 single param");
    }

    @Override
    public boolean insert(ITreeElement key, double[] values, BitSet flags) {
        throw new UnsupportedOperationException("use version with 1 single param");
    }

    @Override
    public <T extends Appendable> T print(T t) {
        return tree.print(t);
    }

    @Override
    public ITreeElement selectDRandom(int index) {
        return tree.selectDRandom(index);
    }

    @Override
    public ITreeElement selectDRandom(ITreeElement from, ITreeElement to, int index) {
        return tree.selectDRandom(from, to, index);
    }

    @Override
    public ITreeElement selectIRandom(int index) {
        return tree.selectIRandom(index);
    }

    @Override
    public ITreeElement selectIRandom(ITreeElement from, ITreeElement to, int index) {
        return tree.selectIRandom(from, to, index);
    }

    @Override
    public ITreeElement selectRandom() {
        return tree.selectRandom();
    }

    @Override
    public ITreeElement selectRandom(ITreeElement from, ITreeElement to) {
        return tree.selectRandom(from, to);
    }

    @Override
    public boolean setBit(ITreeElement key, int bit) {
        return tree.setBit(key, bit);
    }

    @Override
    public int size() {
        return tree.size();
    }

    @Override
    public int size(ITreeElement from, ITreeElement to) {
        return tree.size(from, to);
    }

    @Override
    public double sum(int attrIdx) {
        return tree.sum(attrIdx);
    }

    @Override
    public double sum(ITreeElement from, ITreeElement to, int attrIdx) {
        return tree.sum(from, to, attrIdx);
    }

    @Override
    public double[] sums(int[] indexes) {
        return tree.sums(indexes);
    }

    @Override
    public double[] sums(ITreeElement from, ITreeElement to, int[] indexes) {
        return tree.sums(from, to, indexes);
    }

    @Override
    public boolean update(ITreeElement key, BitSet flags) {
        throw new UnsupportedOperationException("use version with 1 single param");
    }

    @Override
    public boolean update(ITreeElement key, double[] values) {
        throw new UnsupportedOperationException("use version with 1 single param");
    }

    @Override
    public boolean update(ITreeElement key, double[] values, BitSet flags) {
        throw new UnsupportedOperationException("use version with 1 single param");
    }

    @Override
    public Iterator<ITreeElement> iterator() {
        return tree.iterator();
    }

    @Override
    public boolean hasNext() {
        return tree.hasNext();
    }

    @Override
    public ITreeElement next() {
        return tree.next();
    }

    @Override
    public boolean hasPrevious() {
        return tree.hasPrevious();
    }

    @Override
    public ITreeElement previous() {
        return tree.previous();
    }

    @Override
    public int nextIndex() {
        return tree.nextIndex();
    }

    @Override
    public int previousIndex() {
        return tree.previousIndex();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("operation not supported on iterator, use tree.delete");
    }

    @Override
    public void set(ITreeElement e) {
        throw new UnsupportedOperationException("operation not supported on iterator");
    }

    @Override
    public void add(ITreeElement e) {
        throw new UnsupportedOperationException("operation not supported on iterator");
    }

    @Override
    public boolean update(ITreeElement e) {
        return tree.update(e, e.getRates(), e.getFlags());
    }

}

/*

This work is licensed under the Creative Commons Attribution-NonCommercial-
NoDerivatives 4.0 International License. To view a copy of this license,
visit http://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

2013 - 2019; original design and source code by Roel Bakker @ Skardahl / ROEL BAKKER Holding BV
2019 - 2020; refactored by Roel Bakker @
the department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
email: roel.bakker@gmail.com

@see     <a href="http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.680.7561&rep=rep1&type=pdf">
                R. Bakker et al. Rapid Weighted Random Selection in Agent-based Models of Infectious Disease Dynamics Using Augmented B-trees.
               SIMUL 2013 : The Fifth International Conference on Advances in System Simulation</a>


 */

package util.mtree;

import nl.erasmusmc.mgz.magicTree.IMagicTree;

/**
 * A shell around a sim-commons magic tree.
 * 
 * @author Rinke
 */
public interface IElementMagicTree extends IMagicTree<ITreeElement> {

    boolean insert(ITreeElement e);

    boolean update(ITreeElement e);

    double totalContactRate();

}
/*

This work is licensed under the Creative Commons Attribution-NonCommercial-
NoDerivatives 4.0 International License. To view a copy of this license,
visit http://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

2013 - 2019; original design and source code by Roel Bakker @ Skardahl / ROEL BAKKER Holding BV
2019 - 2020; refactored by Roel Bakker @
the department of Public Health,
Erasmus Medical Center,
P.O. Box 2040,
3000 CA Rotterdam
email: roel.bakker@gmail.com
*/

package util.mtree;

import java.util.BitSet;

/**
 * 
 * @author roelb
 * @author rinke (adaption to sim-commons)
 */
public interface ITreeElement extends Comparable<ITreeElement> {

    long key();

    double[] getRates();

    double getRate(int index);

    void setRate(double x, int index);

    /**
     * sets the rate at index <code>index</code> to the value <code>x</code>,
     * and sets all other rates to 0.
     * @param x
     * @param index
     */
    void setRateExclusive(double x, int index);

    BitSet getFlags();

    boolean getFlag(int index);

    void setFlag(int index);

    /**
     * sets the flag at index <code>index</code> to true, and sets all other flags
     * to false.
     * @param index
     */
    void setFlagExclusive(int index);

    void clearFlag(int index);

    double getContactRate();

    void setContactRate(double contactrate);

    public ITreeElement clone();

}

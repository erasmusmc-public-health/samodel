package hospitals;

import java.util.ArrayList;
import java.util.List;

import antibiotics.AntibioticStatesEnum;
import inandoutput.ParameterInput;
import multiCCsimSAbasics.Human;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.math.DistributionType;
import strains.StrainStatesEnum;
import subPopulationGeneral.SubPopulation;
import subPopulationGeneral.SubPopulationTransmissionEvent;
import subPopulationGeneral.SubPopulationTypeEnum;

/**
 * Hospital class, deals with hospital transmission events.
 */
public class HospitalPopulation extends SubPopulation {
	
	private static class Input {
	    private static final int				numberOfWardsPerHospital = ParameterInput.getInstance().getNumberOfWardsPerHospital();
	    private static final double				fractionContactsWithinWards = ParameterInput.getInstance().getFractionOfContactsWithinWards();
	    private static final double				fractionTestedPerHospitalTimeUnit = 
	    											ParameterInput.getInstance().getFractionPatientsTestedForMrsaPerDay() / 
	    											ParameterInput.getInstance().getHospitalStayUnitsPerDay();
	}
    
    private int								nextWardNeedingPersonnel = -1; //so always starts at 0 with assigning, see the setAndGet function
  
    public HospitalPopulation(World world, int iD) {
        super(world, world.getDistributions().drawFromHospitalPatientsContactRateDist(), iD);
        contactRateLevelOfVariation = SubPopulationContactRateLevelOfVariation.valueOf(
        		ParameterInput.getInstance().getHospitalContactRateLevelOfVariation());
        contactRateStructure = SubPopulationContactRateStructuring.valueOf(
        		ParameterInput.getInstance().getHospitalContactRateStructure().toUpperCase());
        alfa = ParameterInput.getInstance().getHospitalContactRateSaturatingAlfa();
        whichContactRate = SubPopulationTypeEnum.HOSPITAL;
    }

    /**
     * Part one of the sub-population daily transmission rate calculation, depends on the level of variation in daily
     * contact rates. Also stores for each individual relative contact rate and susceptibility, for later use in drawing
     * of a susceptible who gets infected. Takes partial population separation into hospital wards into account.
     * @return The sub-population transmission rate assuming linear increase in contacts with sub-population members.
     */
    @Override
    public double calculateRawTransmissionRateFromVariatonLevel(String strain, SubPopulationTypeEnum whichContactRate,
            AntibioticStatesEnum whichInfandSus) {
    	if (Input.numberOfWardsPerHospital > 1) { //check if this per ward calculation is needed, does not seem to have any time-saving effect though.
        double rate = 0.;
        double[] suminf = new double[Input.numberOfWardsPerHospital];
        double[] sumcontact = new double[Input.numberOfWardsPerHospital];
        double sumsus = 0.;
        final double[] newSusceptibilities = new double[nrOfMembers];
        int i = 0;
        switch (contactRateLevelOfVariation) {
	        case SUBPOPULATION: // setting not allowed for hospital population
	        case SUBPOPULATIONMEMBERSC:
	            for (final Human human : members) {
	                if (human.getStrains().get(strain).getState() == StrainStatesEnum.I) {
	                    newSusceptibilities[i] = 0.0;
	                    suminf[human.getHospitalWard()] += human.getStrains().get(strain).getInfectivity(whichInfandSus)
	                            * human.getContactrateSubPop(whichContactRate);
	                    sumcontact[human.getHospitalWard()] += human.getContactrateSubPop(whichContactRate);
	                }
		            if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
		                sumcontact[human.getHospitalWard()] += human.getContactrateSubPop(whichContactRate);
		            }
		            i++;
	            }
		        i = 0;
		        for (final Human human : members) {
		            if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
		                newSusceptibilities[i] = human.getStrains().get(strain).getSusceptibility(whichInfandSus)
		                        * human.getContactrateSubPop(whichContactRate) *
		                        ( Input.fractionContactsWithinWards * suminf[human.getHospitalWard()] / 
		                        		(sumcontact[human.getHospitalWard()] ) +
		                        (1 - Input.fractionContactsWithinWards) * sumArray(suminf) / ( sumArray(sumcontact) )
		                        );
		                sumsus += newSusceptibilities[i];
		            }
		            i++;
		        }
		        rate = nrOfMembers * sumsus;
	            break;
	        case SUBPOPULATIONMEMBERNOSC:
		        for (final Human human : members) {
		            if (human.getStrains().get(strain).getState() == StrainStatesEnum.I) {
		                newSusceptibilities[i] = 0.0;
		                suminf[human.getHospitalWard()] += human.getStrains().get(strain).getInfectivity(whichInfandSus)
		                        * human.getContactrateSubPop(whichContactRate);
		                sumcontact[human.getHospitalWard()] += human.getContactrateSubPop(whichContactRate);
		            }
		            if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
		                sumcontact[human.getHospitalWard()] += human.getContactrateSubPop(whichContactRate);
		            }
		            i++;
		        }
		        i = 0;
		        for (final Human human : members) {
		            if (human.getStrains().get(strain).getState() == StrainStatesEnum.S) {
		                newSusceptibilities[i] = human.getStrains().get(strain).getSusceptibility(whichInfandSus)
		                        * human.getContactrateSubPop(whichContactRate) *
		                        ( Input.fractionContactsWithinWards * suminf[human.getHospitalWard()] / 
		                        (sumcontact[human.getHospitalWard()] - Input.fractionContactsWithinWards
		                        		* human.getContactrateSubPop(whichContactRate)) +
		                        (1 - Input.fractionContactsWithinWards) * sumArray(suminf) / 
		                        (sumArray(sumcontact) - ( 1 - Input.fractionContactsWithinWards) * 
		                        		human.getContactrateSubPop(whichContactRate))
		                        );
		                sumsus += newSusceptibilities[i];
		            }
		            i++;
		        }
		        rate = (nrOfMembers - 1) * sumsus;
		        break;
    	}
        susceptibilities.put(strain, newSusceptibilities);
        return rate;
    	} else {return super.calculateRawTransmissionRateFromVariatonLevel(strain, whichContactRate, whichInfandSus);}
    }

    /**
     * The infection event may be canceled when members are less infectious or susceptible due to antibiotics usage.
     * Note, the last calculated rate must be the most accurate, since in its calculation individual infection rates
     * are stored, for drawing who gets infected.
     * @param e The transmission event.
     */
    @Override
    public void handle(SubPopulationTransmissionEvent e) {
        final double transRateIgnoringAntibiotics = getSubPopulationTransmissionRate(e.getStrainmodel().getName(),
                SubPopulationTypeEnum.HOSPITAL, AntibioticStatesEnum.NOT_TAKING_AB);
        final double transRateWithAntibiotics = getSubPopulationTransmissionRate(e.getStrainmodel().getName(),
                SubPopulationTypeEnum.HOSPITAL, AntibioticStatesEnum.TAKING_AB);
        if (world.nextDouble() <= (transRateWithAntibiotics / transRateIgnoringAntibiotics)) {
            handle(e.getStrainmodel());
        } else {
            updateInfectionRate(e.getStrainmodel().getName());
        }
    }
    
    /*
	 * Selects patients at random and has them tested for MRSA. 
	 */
    public void selectPatientsToTestForMRSA() {
        final List<Human> allPatients = new ArrayList<>();
        for (final Human human : members) {
            if (human.getAntibioticsAndHospitalState().getHospitalState() == HospitalStatesEnum.HOSPITALISED) {
                allPatients.add(human);
            }
        }
    	int howManyTested = world.getMath().getDiscreteDistribution( DistributionType.valueOf( "POISSON" ), null, null,
    			Input.fractionTestedPerHospitalTimeUnit * allPatients.size()).nextInt();
    	for(int i=0; i<howManyTested; i++) {
    		int randomPatient = (int) world.nextDouble() * allPatients.size();
    		allPatients.get(randomPatient).getAntibioticsAndHospitalState().getHospitalProcessor().scheduleMrsaTestResultEvent();
    		allPatients.remove(randomPatient);
    	}
    }

    /**
     * When new hospital personnel is assigned, they are assigned to the next ward in sequence of ward numbers
     * @return number of the ward to assign personnel to
     */
	public int setAndGetNextWardNeedingPersonnel() {
		nextWardNeedingPersonnel = (nextWardNeedingPersonnel + 1) % Input.numberOfWardsPerHospital;
		return nextWardNeedingPersonnel;
	}
	
	private double sumArray(double[] array) {
    	double summed = 0;
    	for (double i : array) {
    	    summed += i;
    	}
    	return summed;
    }

}
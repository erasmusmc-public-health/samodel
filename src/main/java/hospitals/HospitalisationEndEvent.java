package hospitals;

import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.eventmanager.IEventManager;


/**
 * Event that can be scheduled in the eventmanager, to end antibiotics therapy.
 * @author Anneke S. de Vos
 *
 */
 public class HospitalisationEndEvent extends Event implements IEvent {
	    
	private final HospitalTreeElementProcessor patient;
	 
	    public HospitalisationEndEvent(HospitalTreeElementProcessor patient, IEventManager eventManager) {
	        super(eventManager);
	        this.patient = patient;
	    }

	    @Override
	    public void execute() {
	        patient.endHospitalisation();
	    }

}
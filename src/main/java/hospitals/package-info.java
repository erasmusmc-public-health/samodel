/**
 * Classes representing hospitals, and supporting hospital effects. 
 * However, AntibioticsAndHospitalTreeElements contains the tree and so hospital status.
 */
package hospitals;
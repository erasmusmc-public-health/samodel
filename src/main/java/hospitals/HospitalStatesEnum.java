package hospitals;

import inandoutput.ParameterInput;

/**
 * Possible hospital states. There may not be any HOSPITAL_PERSONNEL.
 * So that we do not create an unnecessarily big tree (to be memory efficient), a
 * static numberOfHospitalStatesUsed(), and an adaptable tree index, both depending
 * on actually used states, are added. 
 * @author Anneke S. de Vos
 */
public enum HospitalStatesEnum {
	
    NOT_HOSPITALISED (0), 
    HOSPITAL_PERSONNEL (1),
    HOSPITALISED (1 + (ParameterInput.getInstance().getNumberOfPersonnelPerHospitalPatient() == 0 ? 0 : 1) );
	
    private final static boolean  thereIsHospitalPersonnel = 
    		!(ParameterInput.getInstance().getNumberOfPersonnelPerHospitalPatient() == 0);

    public final static int numberOfHospitalStatesUsed() {
    	return ( 2 + (thereIsHospitalPersonnel ? 1 : 0) ); 
    }

    private final int index;

    private HospitalStatesEnum(int index) {
        this.index = index;
    }
    
    public int index() {
        return index;
    }
}

package hospitals;

import antibiotics.AntibioticStatesEnum;
import inandoutput.ParameterInput;
import multiCCsimSAbasics.AntibioticsHospitalTreeElement;
import multiCCsimSAbasics.Human;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import strains.InfectionStatesEnum;
import strains.StrainTreeElement;
import subPopulationGeneral.SubPopulationTypeEnum;

/**
 * Holds hospital related functions. Magic tree specifics must be handled in the AntibioticsHospitalTreeElement itself.
 * Hospitalization may raise the rate of AB taking, and alter contact rates of individuals. 
 * 
 * @author Anneke S. de Vos
 *
 */
public class HospitalTreeElementProcessor {
	
	private static class Input {
	    private static final double					probabilityForNewContactRateAtNewHospitalisation = 
	    												ParameterInput.getInstance().getProbabilityForNewContactRateAtNewHospitalisation();
	    private static final double					hospitalStayUnitsPerDay = ParameterInput.getInstance().getHospitalStayUnitsPerDay();
	    private static final int					numberOfWardsPerHospital = ParameterInput.getInstance().getNumberOfWardsPerHospital();
	    private static final boolean				searchAndDestroyIsOn = ParameterInput.getInstance().isSearchAndDestroyIsOn();
	    private static final double					fractionRandomTestedForMrsaAtHospitalEntrance = 
	    												ParameterInput.getInstance().getFractionRandomTestedForMrsaAtHospitalEntrance();
	    private static final double					mrsaTestSensitivity = ParameterInput.getInstance().getMrsaTestSensitivity();
	    private static final int					numberOfNormalHouseholds = ParameterInput.getInstance().getNumberOfHouseholds();
	    private static final boolean				familyTestingIsOn = ParameterInput.getInstance().getFamilyTestingDelayInDays() >
	    												ParameterInput.getInstance().getHospitalStayDaysAverage();
	    private static final int					familyTestingDelayInDays = ParameterInput.getInstance().getFamilyTestingDelayInDays();
	    private static final boolean				targetedABTreatmentIsOn = ParameterInput.getInstance().
	    																			isFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn(); 
	    private static final boolean				familyTargetedABTreatmentIsOn = ParameterInput.getInstance().
	    																				isFamilyTestingFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn();
	}
	
	private final AntibioticsHospitalTreeElement treeElement;
    private final Human      	 human;
    
	public HospitalTreeElementProcessor(AntibioticsHospitalTreeElement treeElement) {
		this.treeElement = treeElement;
		this.human = treeElement.getHuman();
	}

    /*
     * Test results duration is rounded to whole hospital stay units + 1, so results become available only at the time people may leave 
     * the hospital, in order that resulting isolation not require extra updates of the HospitalPopulation infection rate. 
     */
    public void scheduleMrsaTestResultEvent() {
    	boolean testResultPositive = testForMrsa();
    	MrsaTestResultEvent mrsaTestEvent = new MrsaTestResultEvent(this, human.getWorld().getEventManager(), testResultPositive);
    	mrsaTestEvent.scheduleRelative(TimeUtilsDays.asDuration(
    			human.getWorld().getDistributions().drawFromMrsaTestDelayInHospitalStayUnitsDist() / Input.hospitalStayUnitsPerDay) + 1);
    }
    
    /*
     * When a patient enters the hospital, hospitalization end is scheduled. Also they may be scheduled for MRSA testing.  
     */
    public void startHospitalisation() {
    	scheduleEndHospitalisationEvent();
    	mrsaTestingOrNotAtHospitalisation();
        human.getWorld().getHospitals().get(human.getAssignedHospital()).join(human);
        treeElement.setHospitalState(HospitalStatesEnum.HOSPITALISED);
        treeElement.updateFlagsRatesTreeHospital();
    	startHospitalisationEffectsOnInfectionRates();
    }
    
    /*
     * Testing of household members occurs only if the index has left the hospital, else family testing is postponed 
     */
    public void householdMrsaTestEvent() {
    	if (treeElement.getHospitalState() == HospitalStatesEnum.NOT_HOSPITALISED) {
    		human.getHousehold().testHouseholdForMRSA();} 
    	else {scheduleHouseholdMrsaTestEvent();}
    }
    
    /*
     * Hospital personnel is treated just like patients are, but without any end to hospitalization scheduled, 
     * also they have a different hospital contact rate, and their other contact rates are unaffected. 
     */
    public void makeHospitalPersonnel(int hospital) {
        human.setHospitalWard(human.getWorld().getHospitals().get(hospital).setAndGetNextWardNeedingPersonnel());
        human.setContactrateHosp(human.getWorld().getDistributions().drawFromHospitalPersonnelContactRateDist());
        human.getWorld().getHospitals().get(hospital).join(human);
        human.setAssignedHospital(hospital);
        treeElement.setHospitalState(HospitalStatesEnum.HOSPITAL_PERSONNEL);
        treeElement.updateFlagsRatesTreeHospital();
    	if (human.isGiveExtraOutput()) {human.getHousehold().SPoutput(",HoPers,", human.getHumanNr(), "CC-0", ",ABs\n");}
    }

    /*
     * When test results become available this changes assumed MRSA status. Isolation (fewer hospital contacts) hinges on this, see Human.
     * Testing positive may lead to future testing of all family members (including the patient). 
     */
    public void mrsaTestResultEvent(boolean testResultPositive) {
    	if (testResultPositive) {
    		human.setAssumedMrsaPositive(true);
        	if (Input.familyTestingIsOn && (human.getHousehold().getID() < Input.numberOfNormalHouseholds)) {
    			scheduleHouseholdMrsaTestEvent();
        	} 
        	if (Input.targetedABTreatmentIsOn && treeElement.getHospitalState() == HospitalStatesEnum.HOSPITALISED) {targetedAB();}
    	} else {human.setAssumedMrsaPositive(false);} 
    }
    
    /*
     * For family members, testing is immediate (as the delay is included in familyTestingDelayInDays), and no family testing can result.
     * Hospital personnel is excluded from having an assumed MRSA+ status (and thereby from isolation), but can receive targeted treatment.    
     */
    public void mrsaTestResultEventForFamily() {
    	boolean testResultPositive = testForMrsa();
    	if (testResultPositive && !(treeElement.getHospitalState() == HospitalStatesEnum.HOSPITAL_PERSONNEL) ) {
    		human.setAssumedMrsaPositive(true);
    		if (Input.familyTargetedABTreatmentIsOn) {targetedAB();}
    	} else {human.setAssumedMrsaPositive(false);} 
    }
    
    /*
     * Ends hospital status, and also hospitalisation effects on contact rates.  
     */
    public void endHospitalisation() {
        human.getWorld().getHospitals().get(human.getAssignedHospital()).leave(human);
    	stopHospitalisationEffectsOnInfectionRates();
    	treeElement.setHospitalState(HospitalStatesEnum.NOT_HOSPITALISED);
        treeElement.updateFlagsRatesTreeHospital();
    }
    
    /*
     * If search and destroy policy is active, those of high risk are set as assumed positive until tested, but individuals may also 
     * still be assumed positive from testing previously. Without search and destroy policy, all individuals are assumed negative until 
     * tested positive during the current hospitalization.
     */
    private void mrsaTestingOrNotAtHospitalisation() {
    	if (Input.searchAndDestroyIsOn) {
    		if (human.isIneFOIRiskgroup()) {human.setAssumedMrsaPositive(true);} 
    		if (human.isAssumedMrsaPositive() || (Input.fractionRandomTestedForMrsaAtHospitalEntrance > human.getWorld().nextDouble())) {
    			scheduleMrsaTestResultEvent();}
    	}
    	else {
    		human.setAssumedMrsaPositive(false);
    		if (Input.fractionRandomTestedForMrsaAtHospitalEntrance > human.getWorld().nextDouble()) {
    			scheduleMrsaTestResultEvent();}
    	}
    }
    
    private boolean testForMrsa() {
    	return human.getOverallInfState()==InfectionStatesEnum.IMRSA  && (Input.mrsaTestSensitivity > human.getWorld().nextDouble());
    }
    
    /* 
     * Hospital stay Time Units are discretized, since we limit the number of moments at which individuals may enter or leave hospital. 
     * As the minimum is 1 hospital time unit, we increase the draw by 1 unit, but we have decreased the user set distribution mean by 
     * one Unit (in ParameterInput.java). To make sure hospital exits occur just after hospital joins, we add 1 to the long
     * representing duration (see HospitalisationEvent.java for further explanation).   
     */
    private void scheduleEndHospitalisationEvent() {
    	HospitalisationEndEvent endHospitalisationEvent = new HospitalisationEndEvent(this, human.getWorld().getEventManager());
    	int howManyHospitalStayUnits = human.getWorld().getDistributions().drawFromHospitalStayInHospitalStayUnitsDist() + 1;
        endHospitalisationEvent.scheduleRelative(TimeUtilsDays.asDuration( (howManyHospitalStayUnits / Input.hospitalStayUnitsPerDay)) + 1);
    }

    private void scheduleHouseholdMrsaTestEvent() {
    	HouseholdMrsaTestEvent householdMrsaTestEvent = new HouseholdMrsaTestEvent(this, human.getWorld().getEventManager());
    	householdMrsaTestEvent.scheduleRelative(TimeUtilsDays.asDuration(Input.familyTestingDelayInDays));
    }
    
    /*
     * Hospitalization starts hospital contacts, and stops general population contacts. 
     * Hospital ward and contact rate may or may not be the same as at the individual's previous visit.
     * Decrease of subpopulation (HH or NH) contacts is done indirectly, by household infection cancellations.
     */
    private void startHospitalisationEffectsOnInfectionRates() {
    	if (human.getContactrateSubPop(SubPopulationTypeEnum.HOSPITAL) == 0 ||
    			Input.probabilityForNewContactRateAtNewHospitalisation > human.getWorld().nextDouble() ) {
            human.setHospitalWard( (int) (Input.numberOfWardsPerHospital * human.getWorld().nextDouble() ) );
            human.setContactrateHosp(human.getWorld().getDistributions().drawFromHospitalPatientsContactRateDist());
    	}
        for (final StrainTreeElement strain : human.getStrains().values()) {
       		strain.startHospitalEffectsOnGPContacts();
        }
        if (human.isGiveExtraOutput()) {updateHouseholdWithExtraOutputInfo(",inHosp,");} 
    }
    
    /*
     * Restarts general population contacts.
     */
    private void stopHospitalisationEffectsOnInfectionRates() {
        for (final StrainTreeElement strain : human.getStrains().values()) {
       		strain.stopHospitalEffectsOnGPContacts();
        }
        if (human.isGiveExtraOutput()) {updateHouseholdWithExtraOutputInfo(",outHosp,");} 
    }
    
    /*
     * If someone was found MRSA positive (see hospital part), AB treatment targeted to MRSA may be started (or if one is already 
     * being treated, the treatment is turned into targeted treatment.). This changes only the clearing probability. 
     */
    private void targetedAB() {
    	treeElement.setTargetedTreatmentIsOn(true);
    	if (treeElement.getAntibioticsState() != AntibioticStatesEnum.TAKING_AB) {
    		treeElement.getAntibioticsProcessor().startAB();
    	}
    }
    
    /*
     * @param newstate What is the new hospital state. For extra output purposes.
     */
    private void updateHouseholdWithExtraOutputInfo(String newstate) {
       		human.getHousehold().SPoutput(newstate, human.getHumanNr(), "CC-0", ",ABs\n");
    }
    
}

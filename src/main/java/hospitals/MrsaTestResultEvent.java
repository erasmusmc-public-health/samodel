package hospitals;

import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.eventmanager.IEventManager;


/**
 * Event that can be scheduled in the eventmanager, to obtain MRSA test results.
 * @author Anneke S. de Vos
 *
 */
 public class MrsaTestResultEvent extends Event implements IEvent {
	    
	private final HospitalTreeElementProcessor patient;
	private final boolean						 testResult;
	 
	    public MrsaTestResultEvent(HospitalTreeElementProcessor patient, IEventManager eventManager, boolean testResult) {
	        super(eventManager);
	        this.patient = patient;
	        this.testResult = testResult;
	    }

	    @Override
	    public void execute() {
	        patient.mrsaTestResultEvent(testResult);
	    }

}
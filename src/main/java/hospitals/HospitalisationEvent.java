package hospitals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import inandoutput.ParameterInput;
import multiCCsimSAbasics.AntibioticsHospitalTreeElement;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.math.DistributionType;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import strains.StrainModel;
import util.mtree.IElementMagicTree;

/**
 * Event that can be scheduled in the eventmanager, to hospitalize a number of individuals. 
 * Each HospitalisationEvent schedules the next event of this type, hospitalisationInterval time later.
 * The number to hospitalize is determined by summed rates and Poisson. Weighted by their hospitalization rates, 
 * individuals to hospitalize are drawn. 1 time-tick after new hospitalizations occur, other individuals leave.
 * Also MRSA test results, possibly resulting in isolation of patients, become available only at these times.
 * We need only update the hospital infection rates once per hospitalisationInterval therefore, which we do 1 time 
 * tick after hospital exits and test results occur. This class also takes care of recruiting hospital personnel. 
 * 
 *  @author Anneke S. de Vos
 */
public class HospitalisationEvent extends Event implements IEvent {
	
    private static final Logger     LOGGER = LoggerFactory.getLogger(World.class);
    
	private static class Input {
		private static final double 	hospitalisationTimeUnitsPerDay = ParameterInput.getInstance().getHospitalStayUnitsPerDay();
		private static final long    	hospitalisationInterval = TimeUtilsDays.asDuration(1 / hospitalisationTimeUnitsPerDay, 0, 0, 0);
		private final static double     wantedNumberOfHospitalPatientsPerPersonnel = 1 / ParameterInput.getInstance().getNumberOfPersonnelPerHospitalPatient();
		private final static int        numberOfHouseholds = ParameterInput.getInstance().getNumberOfHouseholds();
	}
	
    private final World      		world;
	private final IElementMagicTree tree;
    private final int 				treeIndexHospitalisationRate  = AntibioticsHospitalTreeElement.treeIndexDailyHospitalisationRate;
    private boolean					isHospitalsNeedsUpdating = false;

	public HospitalisationEvent(World world) {
	      super(world.getEventManager());
	      this.world = world;
	      this.tree = world.getGeneralTree();
	}
    
    /**
	 * A boolean field isHospitalsNeedsUpdating is used to switch between two event options, hospitalizations and hospital 
	 * updating. New hospitalizations occur every hospitalisationInterval. 1 time tick later individuals may leave their 
	 * hospitals (if done before, we would slightly overestimate the total rate of entering hospitals). So we schedule 
	 * hospital infection rates updating 2 time ticks later than hospitalizations. 
	 */
    @Override
    public void execute() {
    	if (isHospitalsNeedsUpdating) {
        	updateHospitalInfectionrates();
        	this.isHospitalsNeedsUpdating = false;
            scheduleRelative(Input.hospitalisationInterval - 2);
    	} else {
    		checkNumberOfPersonnelAndRecruitIfNeeded();
    		selectPatientsToTestForMrsa();
    		hospitaliseIndividuals();
        	this.isHospitalsNeedsUpdating = true;
	        scheduleRelative(2);
    	}
    }
    
    private void hospitaliseIndividuals(){
    	int howManyToHospital = howManyHospitalisations();
    	for(int i=0; i<howManyToHospital; i++) {
            final AntibioticsHospitalTreeElement treeElement = (AntibioticsHospitalTreeElement) tree.selectDRandom(treeIndexHospitalisationRate);
            treeElement.getHospitalProcessor().startHospitalisation();
    	}
    }
    
    private int howManyHospitalisations(){
    	double sumRates = sumRates();
    	int howManyToHospital = world.getMath().getDiscreteDistribution( DistributionType.valueOf( "POISSON" ), null, null,
        		sumRates / Input.hospitalisationTimeUnitsPerDay ).nextInt();
    	return howManyToHospital;
    }
    
    /**
     * Function to have a fraction of patients tested for MRSA. 
     */
    public void selectPatientsToTestForMrsa() {
    	for (HospitalPopulation hospital : world.getHospitals()) {
    		hospital.selectPatientsToTestForMRSA();
    	}
    }
    
    public double sumRates() {
    	return tree.sum(treeIndexHospitalisationRate);
    }
    
    public void updateHospitalInfectionrates() {
        for (final StrainModel strain : world.getStrainmodels().getModels().values()) {
        	for (int j = 0; j< ParameterInput.getInstance().getNumberOfHospitals(); j++) {
        		world.getHospitals().get(j).updateInfectionRate(strain.getName());
        	}
        }
    }
    
	/**
     * This function checks and if needed recruits new personnel for hospitals, if the patient to personnel ratio is too high. 
     * Personnel is never laid off, so as hospital occupancy fluctuates and personnel is recruited at peak hours,
     * we do overshoot the parameter input numberOfPersonnelPerHospitalPatient, especially in case of small hospitals.
     * Warns and breaks in case we run out of potential personnel. 
     */
    public void checkNumberOfPersonnelAndRecruitIfNeeded() {
    	int i = 0;
    	int successfulTries = 0;
    	int failedTries = 0;
    	for (HospitalPopulation hospital : world.getHospitals()) {
    		while (hospital.fractionPatientsToPersonnel() > Input.wantedNumberOfHospitalPatientsPerPersonnel) {
               	final AntibioticsHospitalTreeElement treeElement = (AntibioticsHospitalTreeElement) 
                		world.getGeneralTree().selectIRandom(HospitalStatesEnum.NOT_HOSPITALISED.index());
               	if( treeElement.getHuman().getHousehold().getID() < Input.numberOfHouseholds &&
               			!treeElement.getHuman().isNursingHomePersonnel() ) {
               		treeElement.getHospitalProcessor().makeHospitalPersonnel(i); 
               		successfulTries++;
               	} else {failedTries++; if ( (failedTries - successfulTries) > 100) {
               		LOGGER.error("WARNING: ran out of general population to recruit as hospital personnel! \n"
               				+ "   numberOfPersonnelPerHospitalPatient target not reached!"); break;}
               	};
    		}
    		i++;
    	}
    }
}
   
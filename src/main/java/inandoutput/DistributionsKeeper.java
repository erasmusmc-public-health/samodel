package inandoutput;

import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.math.DistributionType;
import nl.erasmusmc.mgz.math.IContinuousDistribution;


/**
 * Class to store model distributions, to declutter other classes. 
 * @author Anneke S. de Vos
 *
 */
public class DistributionsKeeper {
	
    private final World                    world;
    private final IContinuousDistribution  cleardurDist;
    private final IContinuousDistribution  infectiousnessDist;
    private final IContinuousDistribution  susceptibilityDist;
    private final IContinuousDistribution  contactratehhdist;
    private final IContinuousDistribution  contactratenhdist;
    private final IContinuousDistribution  contactratenhforpersonneldist;
    private final IContinuousDistribution  contactratepopdist;
    private final IContinuousDistribution  hospitalPatientsContactRateDist;
    private final IContinuousDistribution  hospitalPersonnelContactRateDist;
    private final IContinuousDistribution  dailyHospitilisationDist;
    private final IContinuousDistribution  dailyABUseRateDist;
    
    public DistributionsKeeper(World world) {
    	this.world = world;
        cleardurDist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getInfectionDurationDistribution().toUpperCase()),
                null, null, ParameterInput.getInstance().getInfectionDurationDistrbutionParameters());
        infectiousnessDist = world.getMath().getContinuousDistribution(DistributionType.CONSTANT, null, null, 1.0);
        susceptibilityDist = world.getMath().getContinuousDistribution(DistributionType.CONSTANT, null, null, 1.0);
        contactratehhdist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getContactrateHouseholdDistribution().toUpperCase()), 
                null, null, ParameterInput.getInstance().getContactrateHouseholdDistributionParameters());
        contactratenhdist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getContactrateNursingHomeDistribution().toUpperCase()),
                null, null, ParameterInput.getInstance().getContactrateNursingHomeDistributionParameters());
        contactratenhforpersonneldist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getContactrateNursingHomeDistribution().toUpperCase()),
                null, null, ParameterInput.getInstance().getContactrateNursingHomePersonnelDistributionParameters());
        contactratepopdist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getContactratePopulationDistribution().toUpperCase()), 
                null, null, ParameterInput.getInstance().getContactratePopulationDistributionParameters());
        hospitalPatientsContactRateDist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getContactrateHospitalDistribution().toUpperCase()), 
                null, null, ParameterInput.getInstance().getContactrateHospitalPatientsDistributionParameters());
        hospitalPersonnelContactRateDist = world.getMath().getContinuousDistribution(
        		DistributionType.valueOf( ParameterInput.getInstance().getContactrateHospitalDistribution().toUpperCase() ), null, null,
        		ParameterInput.getInstance().getContactrateHospitalPersonnelDistributionParameters() );
        dailyHospitilisationDist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getHospitalisationRateDistribution().toUpperCase()),
                null, null, ParameterInput.getInstance().getDailyHospitalisationRateDistributionParameters());
        dailyABUseRateDist = world.getMath().getContinuousDistribution(
                DistributionType.valueOf(ParameterInput.getInstance().getAntibioticsUsageDistribution().toUpperCase()),
                null, null, ParameterInput.getInstance().getAntibioticsDailyUsageDistributionParameters());
    }

	public double drawFromCleardurDist() {
		return cleardurDist.nextDouble();
	}
	
	public double drawFromContactratehhdist() {
		return contactratehhdist.nextDouble();
	}

	public double drawFromContactratenhdist() {
		return contactratenhdist.nextDouble();
	}

	public double drawFromContactratenhforpersonneldist() {
		return contactratenhforpersonneldist.nextDouble();
	}

	public double drawFromContactratepopdist() {
		return contactratepopdist.nextDouble();
	}

	public double drawFromDailyABUseRateDist() {
		return dailyABUseRateDist.nextDouble();
	}

	public double drawFromDailyHospilisationDist() {
		return dailyHospitilisationDist.nextDouble();
	}

	public double drawFromHospitalPatientsContactRateDist() {
		return hospitalPatientsContactRateDist.nextDouble();
	}

	public double drawFromHospitalPersonnelContactRateDist() {
		return hospitalPersonnelContactRateDist.nextDouble();
	}
	public int drawFromHospitalStayInHospitalStayUnitsDist() {
        switch (ParameterInput.getInstance().getHospitalStayDaysDistribution().toUpperCase()) {
        case "CONSTANT":
            return ParameterInput.getInstance().getHospitalStayDurationInHospitalStayUnitsAverage(); 
        default:
			return world.getMath().getDiscreteDistribution(
        				DistributionType.valueOf( ParameterInput.getInstance().getHospitalStayDaysDistribution().toUpperCase() ), 
        				null, null, ParameterInput.getInstance().getHospitalStayDurationInHospitalStayUnitsDistributionParameters() ).nextInt();
        }
	}

	public double drawFromInfectionDurationAverageDistribution() {
		return ParameterInput.getInstance().getInfectionDurationAveragePerTypeInDays()[
		            world.getMath().getDiscreteDistribution(DistributionType.CUSTOM, null, null,
		            ParameterInput.getInstance().getInfectionDurationIndividualProbabilitiesToBeOfType()).nextInt()];
	}
	
	public double drawFromInfectiousnessDist() {
		return infectiousnessDist.nextDouble();
	}
	
	public int drawFromMrsaTestDelayInHospitalStayUnitsDist() {
        switch (ParameterInput.getInstance().getTimeDelayForMrsaTestResultsDistribution().toUpperCase()) {
        case "CONSTANT":
            return ParameterInput.getInstance().getTimeDelayForMrsaTestResultsInHospitalStayUnitsAverage(); 
        default:
			return world.getMath().getDiscreteDistribution(
					DistributionType.valueOf( ParameterInput.getInstance().getTimeDelayForMrsaTestResultsDistribution().toUpperCase() ), 
					null, null, ParameterInput.getInstance().getTimeDelayForMrsaTestResultsInHospitalStayUnitsDistributionParameters() ).nextInt();
        }
    }
	
	public double drawFromSusceptibilityDist() {
		return susceptibilityDist.nextDouble();
	}

}

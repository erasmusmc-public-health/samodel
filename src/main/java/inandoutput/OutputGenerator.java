package inandoutput;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hospitals.HospitalStatesEnum;
import multiCCsimSAbasics.World;
import nl.erasmusmc.mgz.eventmanager.Event;
import nl.erasmusmc.mgz.eventmanager.IEvent;
import nl.erasmusmc.mgz.math.DistributionType;
import nl.erasmusmc.mgz.math.IContinuousDistribution;
import nl.erasmusmc.mgz.utils.process.impl.InitializableProcess;
import nl.erasmusmc.mgz.utils.process.model.IInitializableProcess;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;
import strains.StrainStatesEnum;

/**
 * Class to generate population level model output to a .csv (comma delimited text) file.
 * Fails if this output file is currently opened in another program!
 *
 * @author Anneke S. de Vos
 */
 

public class OutputGenerator extends InitializableProcess implements IInitializableProcess {

    /**
     * Event that can be scheduled in the eventmanager, to execute output events. 
     * @author Anneke S. de Vos
     *
     */
    public class OutputEvent extends Event implements IEvent {
        public OutputEvent(World world) {
			super(world.getEventManager());
		}
        @Override
        public void execute() {
        	outputEvent();
        }
    }
    
	private static final Logger     LOGGER = LoggerFactory.getLogger(World.class);
	private static class Input {
	    private static final String 	OUTPUTFOLDER = "src/main/output/";
	    private static final String		OUTPUTFILENAME = ParameterInput.getInstance().getOutputFileName();
	    
	    private static final String    	filePath = Paths.get(System.getProperty("user.dir"), OUTPUTFOLDER + 
	    										OUTPUTFILENAME + "_" + ParameterInput.getInstance().getTimeStamp()).toString();
	    private static final long    	outputGenerationInterval = ParameterInput.getInstance().getOgInterval();
	    private static final boolean	isGiveOutputPerNHandH = ParameterInput.getInstance().isGiveOutputPerNursingHomeAndHospital();
	    private static final boolean	isGiveExtraOutput = ParameterInput.getInstance().getExtraOutputMaxHouseholdID() +
	    									ParameterInput.getInstance().getExtraOutputMaxNursingHomeID() > 0;
	}
	public static String getFilePath() {
    	return Input.filePath;
    }
    
    private final World      		world;
    private final FileWriter 		csvWriter;
    private final StringBuilder 	sb;
    private final OutputEvent		outputEvent; 
    
    public OutputGenerator(World world) throws IOException {
        this.world = world;
        if (world.getRepeat()==0) {new File(Input.filePath).mkdirs();}
        this.csvWriter = new FileWriter(Input.filePath + "/" + Input.OUTPUTFILENAME + "_" + world.getRepeat() + ".csv");
        this.sb = new StringBuilder();
    	this.outputEvent = new OutputEvent(world);
    }
    
    /**
     * The periodic output of data is ended by writing the stringbuilder content to a .csv, and closing this .csv file.
     */
    private void endOutput() {
        try {
        	csvWriter.append(sb.toString());
            csvWriter.flush();
            csvWriter.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * If executed, more detailed household level event data is gathered in Household.java, 
     * and written here to "outputFileName"_HHdata.csv.
     */
    private void hHlevelOutput() {
        final Path filePathhhd = Paths.get(Input.filePath + "/" + Input.OUTPUTFILENAME + "_" + world.getRepeat() + "_HHdata.csv");
        FileWriter csvWriterhhd;
        try {
            csvWriterhhd = new FileWriter(filePathhhd.toString());
            csvWriterhhd.append("time,hhID,hhSize,humanID,new state,CC,how\n");
            // loop over wanted households
            for (int i = 0; i < Math.min(ParameterInput.getInstance().getNumberOfHouseholds(),
            		ParameterInput.getInstance().getExtraOutputMaxHouseholdID()); i++) {
                csvWriterhhd.append(world.getHouseholds().get(i).getHhOutPut());
            }
            // loop over wanted nursing homes
            for (int i = ParameterInput.getInstance().getNumberOfHouseholds();
            		i < Math.min(ParameterInput.getInstance().getNumberOfHouseholds() + 
            				ParameterInput.getInstance().getNumberOfNursingHomes(), 
            				ParameterInput.getInstance().getNumberOfHouseholds() +
            				ParameterInput.getInstance().getExtraOutputMaxNursingHomeID()); i++) {
                csvWriterhhd.append(world.getHouseholds().get(i).getHhOutPut());
            }
            csvWriterhhd.flush();
            csvWriterhhd.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Parameters are saved to a .txt, the start of the output is generated, and then a first output event is scheduled at time 0
     */
    public void initialize() {
        	super.initialize();
        	if (world.getRepeat()==0) {outputParameters();}
        	startOutput();
            outputEvent.scheduleAbsolute(0);
    }
    
    /**
     * One line of output is added to the StringBuilder.
     */
    private void output() {
        sb.append(TimeUtilsDays.timePointAsDays(world.getEventManager().now()) - world.getStartTimeAsDay() ); 
        for (final String strainName : world.getStrainmodels().getModels().keySet()) { // for each SA CC
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNpopfailinfected());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNinfected());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNinfectedGeneralpopulation());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNinfectedHousehold());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNinfectedNursinghome());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNinfectedHospital());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNinfectedeFOI());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNcleared());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNclearedNaturally());
        	sb.append(",").append(world.getStrainmodels().getModels().get(strainName).getNclearedAntibiotics());
        	sb.append(",").append(world.tree(strainName).countFlag(StrainStatesEnum.S.ordinal()));
        	sb.append(",").append(world.tree(strainName).countFlag(StrainStatesEnum.I.ordinal()));
        	sb.append(",").append( totalNursHomeUninf(strainName) );
        	sb.append(",").append( totalNursHomeInf(strainName) );
        	sb.append(",").append( totalNursHomeUninfNHP(strainName) );
        	sb.append(",").append( totalNursHomeInfNHP(strainName) );
         	sb.append(",").append( totalHospUninf(strainName) );
        	sb.append(",").append( totalHospInf(strainName) );
         	sb.append(",").append( totalHospUninfHP(strainName) );
        	sb.append(",").append( totalHospInfHP(strainName) );
        	if (Input.isGiveOutputPerNHandH) {
		        for (int i = 0; i < world.getHospitals().size(); i++) {
	                sb.append(",").append(world.getHospitals().get(i).nUninfected(strainName));
	                sb.append(",").append(world.getHospitals().get(i).nInfected(strainName));	
		        }
		        for (int i = 0; i < ParameterInput.getInstance().getNumberOfNursingHomes(); i++) {
		        	sb.append(",").append(world.getHouseholds().get(i+ParameterInput.getInstance().getNumberOfHouseholds()).nUninfected(strainName));
		        	sb.append(",").append(world.getHouseholds().get(i+ParameterInput.getInstance().getNumberOfHouseholds()).nInfected(strainName));
		        }
        	}
        }
        sb.append("\n");
    }
    
    /**
     * At each output event, one line of output is generated. Also, a next outputEvent is scheduled interval time later,
     * unless that would be after stopTime, then an event is scheduled at stopTime instead. At stopTime, we endOutput(), 
     * and, if wanted, also additional detailed household level output is generated.  
     */
    private void outputEvent() {
    	output();
        if (world.getEventManager().now() + Input.outputGenerationInterval <= world.getStopTime()) {
            	outputEvent.scheduleRelative(Input.outputGenerationInterval);
        } else if (world.getEventManager().now() < world.getStopTime()) {
            	outputEvent.scheduleAbsolute(world.getStopTime());
        } else {// so when eventManager.now() == world.getStopTime()
            	endOutput();
                if (Input.isGiveExtraOutput) {
                	LOGGER.info("writing extra detailed output.");
                    hHlevelOutput();
                }
        }
    }
    
    /**
     * The model parameters are saved to "outputFileName"_parameters.txt.
     */
    private void outputParameters() {
        final Path filePathparameters = Paths.get(Input.filePath + "/" + Input.OUTPUTFILENAME + "_parameters.txt");
    	try {
    		FileWriter csvWriterparameters = new FileWriter(filePathparameters.toString());
   	 		csvWriterparameters.append(ParameterInput.getInstance().toString()).append("\n");
    		csvWriterparameters.flush();
    		csvWriterparameters.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * The output is started by generating column headings which are added to the StringBuilder.
     */
    private void startOutput() {
            sb.append("time_in_days");
            for (final String strainName : world.getStrainmodels().getModels().keySet()) { // for each SA CC
                sb.append(",").append(strainName).append("_cumNrPopFailInf");
                sb.append(",").append(strainName).append("_cumNrInf");
                sb.append(",").append(strainName).append("_cumNrInf_GP");
                sb.append(",").append(strainName).append("_cumNrInf_HH");
                sb.append(",").append(strainName).append("_cumNrInf_NH");
                sb.append(",").append(strainName).append("_cumNrInf_Ho");
                sb.append(",").append(strainName).append("_cumNrInf_eFOI");
                sb.append(",").append(strainName).append("_cumNrClear");
                sb.append(",").append(strainName).append("_cumNrClear_nat");
                sb.append(",").append(strainName).append("_cumNrClear_AB");
                sb.append(",").append(strainName).append("_nrUninf_All");
                sb.append(",").append(strainName).append("_nrInf_All");
                sb.append(",").append(strainName).append("_nrUninf_AllNHpat");
                sb.append(",").append(strainName).append("_nrInf_AllNHpat");
                sb.append(",").append(strainName).append("_nrUninf_AllNHPers");
                sb.append(",").append(strainName).append("_nrInf_AllNHPers");
                sb.append(",").append(strainName).append("_nrUninf_AllHospPat");
                sb.append(",").append(strainName).append("_nrInf_AllHospPat");
                sb.append(",").append(strainName).append("_nrUninf_AllHospPers");
                sb.append(",").append(strainName).append("_nrInf_AllHospPers");
            	if (Input.isGiveOutputPerNHandH) {
	    	        for (int i = 0; i < world.getHospitals().size(); i++) {
	                    sb.append(",").append(strainName).append("_nrUninf_Hosp").append(i);
	                    sb.append(",").append(strainName).append("_nrInf_Hosp").append(i);	
	    	        }
	    	        for (int i = 0; i < ParameterInput.getInstance().getNumberOfNursingHomes(); i++) {
	                    sb.append(",").append(strainName).append("_nrUninf_NHo").append(i);
	                    sb.append(",").append(strainName).append("_nrInf_NHo").append(i);	
	    	        }
            	}
            }
            sb.append("\n");
    }
    
    /**
     * Unreachable code, to give direct output of distributions. For testing purposes only.
     */
    private void testDistribution() {
        final Path filePathhhdTest = Paths.get(Input.filePath + "/" + Input.OUTPUTFILENAME + "_" + world.getRepeat()  + "_TestDistribution.csv");
        FileWriter csvWriterhhdTest;
        try {
            csvWriterhhdTest = new FileWriter(filePathhhdTest.toString());
            final IContinuousDistribution cleardurtest1_1 = world.getMath().getContinuousDistribution(
            		DistributionType.GAMMA, null, null, 1000, 1);
            final IContinuousDistribution cleardurtest1_2 = world.getMath().getContinuousDistribution(
            		DistributionType.GAMMA, null, null, 1000, 10);
            final IContinuousDistribution cleardurtest2_1 = world.getMath().getContinuousDistribution(
            		DistributionType.GAMMA, null, null, 1000, 20);
            final IContinuousDistribution cleardurtest10_1 = world.getMath().getContinuousDistribution(
            		DistributionType.GAMMA, null, null, 1000, 1000000);
            // header for .csv
            csvWriterhhdTest.append("mean 1 sd 1, mean 1 sd 2, mean 2 sd 1, mean 10 sd 1 \n");
            for (int i = 0; i < 1000; i++) {
                csvWriterhhdTest.append(cleardurtest1_1.nextDouble() + "," +
                        cleardurtest1_2.nextDouble() + "," +
                        cleardurtest2_1.nextDouble() + "," +
                        cleardurtest10_1.nextDouble() + "\n");
            }
            csvWriterhhdTest.flush();
            csvWriterhhdTest.close();
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
    
    private int totalHospInf(String strainName) {
    	int sumInf = 0;
        for (int i = 0; i < world.getHospitals().size(); i++) {
            sumInf += world.getHospitals().get(i).nInfected(strainName, HospitalStatesEnum.HOSPITALISED);	
        }
        return sumInf;
    }
    
    private int totalHospInfHP(String strainName) {
    	int sumInf = 0;
        for (int i = 0; i < world.getHospitals().size(); i++) {
            sumInf += world.getHospitals().get(i).nInfected(strainName, HospitalStatesEnum.HOSPITAL_PERSONNEL);	
        }
        return sumInf;
    }
    
    private int totalHospUninf(String strainName) {
    	int sumUninf = 0;
        for (int i = 0; i < world.getHospitals().size(); i++) {
        	sumUninf += world.getHospitals().get(i).nUninfected(strainName, HospitalStatesEnum.HOSPITALISED);
        }
        return sumUninf;
    }
    
    private int totalHospUninfHP(String strainName) {
    	int sumUninf = 0;
        for (int i = 0; i < world.getHospitals().size(); i++) {
        	sumUninf += world.getHospitals().get(i).nUninfected(strainName, HospitalStatesEnum.HOSPITAL_PERSONNEL);
        }
        return sumUninf;
    }

    private int totalNursHomeInf(String strainName) {
    	int sumInf = 0;
        for (int i = 0; i < ParameterInput.getInstance().getNumberOfNursingHomes(); i++) {
        	sumInf += world.getHouseholds().get(i+ParameterInput.getInstance().getNumberOfHouseholds()).nInfected(strainName, false);
        }
        return sumInf;
    }

    private int totalNursHomeInfNHP(String strainName) {
    	int sumInf = 0;
        for (int i = 0; i < ParameterInput.getInstance().getNumberOfNursingHomes(); i++) {
        	sumInf += world.getHouseholds().get(i+ParameterInput.getInstance().getNumberOfHouseholds()).nInfected(strainName, true);
        }
        return sumInf;
    }

    private int totalNursHomeUninf(String strainName) {
    	int sumUninf = 0;
        for (int i = 0; i < ParameterInput.getInstance().getNumberOfNursingHomes(); i++) {
        	sumUninf += world.getHouseholds().get(i+ParameterInput.getInstance().getNumberOfHouseholds()).nUninfected(strainName, false);
        }
        return sumUninf;
    }
    
    private int totalNursHomeUninfNHP(String strainName) {
    	int sumUninf = 0;
        for (int i = 0; i < ParameterInput.getInstance().getNumberOfNursingHomes(); i++) {
        	sumUninf += world.getHouseholds().get(i+ParameterInput.getInstance().getNumberOfHouseholds()).nUninfected(strainName, true);
        }
        return sumUninf;
    }

}

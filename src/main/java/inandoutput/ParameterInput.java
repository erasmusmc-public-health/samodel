package inandoutput;

import java.io.IOException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import nl.erasmusmc.mgz.utils.CollectUtils;
import nl.erasmusmc.mgz.utils.time.TimeUtilsDays;

// option to ignore when the .yaml has too many parameters.
//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
//@JsonIgnoreProperties(ignoreUnknown = true)

/**
 * Class to read in the model parameters from a yaml file. Upon first construction, sets a field 'instance' that holds all parameters
 * that can then be directly called from ParameterInput with getInstance(). Class must include standard getters for all parameters,
 * and the parameters are read only once per run (throws an error otherwise). We do not include setters. All parameters in the yaml file 
 * need to be fields of this class or an error results (unless set to ignore, see above), but the reverse, fields in this class not 
 * mentioned in the yaml, are allowed! These are either set here or left at default value (so 0 for numbers, empty for strings etc.).
 *
 * @author Anneke S. de Vos and Stefan Payralbe
 *
 */

public class ParameterInput {

    // field to be filled with an instance of this class upon first reading in of the parameters, see the constructor below
    private static ParameterInput instance = null;

    public static ParameterInput getInstance() {
        return instance;
    }

    private String       outputFileName;
    private boolean      giveOutputPerNursingHomeAndHospital;
    private int          extraOutputMaxHouseholdID;
    private int          extraOutputMaxNursingHomeID;
    private int          numberOfRepeatRuns;
    private Long         seed;
    private int          numberOfHouseholds;
    private double[]     householdSizeProbabilities;
    private String       simulationDurationUnit;
    private double       simulationDuration;
    private String       outputIntervalUnit;
    private double       outputInterval;
    private int          numberOfStrains;
    private int          numberOfSusceptibleStrains;
    private double[]     fractionInitiallyInfectedPerStrain;
    private double       fractionRiskgroup;
    private double[]     externalForceOfInfectionPerStrain;
    private double[]     infectionProbabilitiesPerStrain;
    private double[]     clearingByABProbabilitiesPerStrain;
    private String       antibioticsUsageDistribution;
    private double       antibioticsYearlyUsageAverage;
    private double       antibioticsYearlyUsageDeviation;
    private double       antibioticsTherapyLengthInDays;
    private double       antibioticsInfectivityPow;
    private double       antibioticsSusceptibilityPow;
    private double       afterAntibioticsAlteredSusceptibilityPeriodInDays;
    private double       afterAntibioticsSusceptibilityMultiplier;
    private String       infectionDurationDistribution;
    private double       infectionDurationDeviation;
    private double[]     infectionDurationIndividualProbabilitiesToBeOfType;
    private double[]     infectionDurationAveragePerTypeInDays;
    private String       contactratePopulationDistribution;
    private double       contactratePopulationDailyAverage;
    private double       contactratePopulationDeviation;
    private String       householdContactRateLevelOfVariation;
    private String       householdContactRateStructure;
    private double       householdContactRateSaturatingAlfa;
    private String       contactrateHouseholdDistribution;
    private double       contactrateHouseholdDailyAverage;
    private double       contactrateHouseholdDeviation;
    private int          numberOfHospitals;
    private int          numberOfWardsPerHospital;
    private double       fractionOfContactsWithinWards;
    private String       hospitalisationRateDistribution;
    private double       hospitalisationYearlyAverage;
    private double       hospitalisationRateDeviation;
    private int          hospitalStayUnitsPerDay;
    private String       hospitalStayDaysDistribution;
    private double       hospitalStayDaysAverage;
    private double       hospitalStayDaysDeviation;
    private String       hospitalContactRateLevelOfVariation;
    private final String hospitalContactRateStructure         = "CONSTANT"; // constraining this to
                                                                            // constant as most
                                                                            // logical option (and
    // as we cannot recalculate the contact rate of alternatives to be per individual, as hospital size is variable over time).
    private final double hospitalContactRateSaturatingAlfa    = 0.;         // unused, kept for
                                                                            // consistency / future
                                                                            // change
    private String       contactrateHospitalDistribution;
    private double       contactrateHospitalPatientsDailyAverage;
    private double       contactrateHospitalPatientsDeviation;
    private double       probabilityForNewContactRateAtNewHospitalisation;
    private double       householdContactRateMultiplierForHospitalised;
    private double       generalPopulationContactRateMultiplierForHospitalised;
    private double       antibioticsHospitalExtraYearlyUsageAverage;
    private double       numberOfPersonnelPerHospitalPatient;
    private double       contactrateHospitalPersonnelDailyAverage;
    private double       contactrateHospitalPersonnelDeviation;
    private int          numberOfNursingHomes;
    private int[]        nursingHomeSizeOptions;
    private double[]     nursingHomeSizeProbabilities;
    private String       nursingHomeContactRateLevelOfVariation;
    private final String nursingHomeContactRateStructure      = "CONSTANT"; // constraining this to
                                                                            // constant as most
                                                                            // logical easy option.
    private final double nursingHomeContactRateSaturatingAlfa = 0.;         // unused, kept for
                                                                            // consistency / future
                                                                            // change
    private String       contactrateNursingHomeDistribution;
    private double       contactrateNursingHomeDailyAverage;
    private double       contactrateNursingHomeDeviation;
    private double       nursinghomeContactratePopulationMultiplier;
    private double       nursinghomeAntibioticsExtraYearlyUsageAverage;
    private double       nursinghomeHospitalisationExtraYearlyAverage;
    private double       numberOfPersonnelPerNursingHomeInhabitant;
    private double       contactrateNursingHomePersonnelDailyAverage;
    private double       contactrateNursingHomePersonnelDeviation;
    private boolean      searchAndDestroyIsOn;
    private int          familyTestingDelayInDays;
    private double       fractionRandomTestedForMrsaAtHospitalEntrance;
    private double       fractionPatientsTestedForMrsaPerDay;
    private String       timeDelayForMrsaTestResultsDistribution;
    private double       timeDelayForMrsaTestResultsDaysAverage;
    private double       timeDelayForMrsaTestResultsDeviation;
    private double       mrsaTestSensitivity;
    private double       fractionOfHospitalContactsRemainingDespiteIsolation;
    private boolean      familyTestingFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn;
    private boolean      foundMrsaPositiveTargetedAntibioticsTreatmentIsOn;
    private double       clearingByABProbabilitiesTargetedIncreaseParameter;
    private double       eventsWaitPeriodInDays;

    private final String 		timeStamp        = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
    
    private final double		daysPerYear 	 = 365.2425; // each year as set in TimeUtilsDays (EventManager) has 365.2425 days, 

    // Without a default constructor, Jackson will throw an exception
    public ParameterInput() {
    }

    /**
     * Reading of parameters from a yaml file located in src/main/resources. Throws an error when called twice in one model run.
     * Returns an instance of the class but also sets an instance field that can be called directly from this class.
     * Also throws errors for disallowed parameter settings.
     * @param parameterFilename The parameter-filename.
     * @return The parameters as a ModelParameters instance object.
     * @throws IOException Exceptions occur when the parameter file is not found, or if parameters present in the .yaml are not
     * in this class ParameterInput.java.
     */
    public ParameterInput parameterRead(String parameterFilename) throws IOException, ExceptionInInitializerError {
        if (instance != null) {
            throw new ExceptionInInitializerError("Parameters have already been set");
        }
        // Needing java.io.Path and java.io.Paths to get to the file location:
        final Path filePath = Paths.get(System.getProperty("user.dir"), "src/main/resources/", parameterFilename);
        final ObjectMapper om = new ObjectMapper(new YAMLFactory());
        final ParameterInput parameterInput = om.readValue(filePath.toFile(), ParameterInput.class);
        instance = parameterInput;
        checkParameterSettings();
        return instance;
    }
    
    /**
     * Certain parameter settings or combinations of settings are not allowed.
     * @throws ExceptionInInitializerError With custom error descriptions for wrong parameter settings.
     */
    public void checkParameterSettings() throws ExceptionInInitializerError {
        if ((instance.householdContactRateLevelOfVariation.toUpperCase().equals("SUBPOPULATION") ||
                instance.nursingHomeContactRateLevelOfVariation.toUpperCase().equals("SUBPOPULATION"))
                && instance.householdContactRateMultiplierForHospitalised != 1.0) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "If the level of variation for households or nursing homes is set to SUBPOPULATION, hospitalisation \n"
                    + "cannot affect an individual's rate of contacts in the model. Please either set \n"
                    + "householdContactRateLevelOfVariation and nursingHomeContactRateLevelOfVariation to SUBPOPULATIONMEMBERSC \n"
                    + "or SUBPOPULATIONMEMBERNOSC, or set householdContactRateMultiplierForHospitalised to 1.0");
        }
        if (instance.antibioticsInfectivityPow < 0.0 || instance.antibioticsSusceptibilityPow < 0.0) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "As the model assumes that antibiotics usage decreases the within subgroup infection rate, neither \n"
                    + "the antibioticsInfectivityPow nor the antibioticsSusceptibilityPow may be set to \n"
                    + "be smaller than 0.");
        }
        if (instance.householdContactRateMultiplierForHospitalised > 1.0) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "As the model assumes hospitalisation decreases the household infection level, the \n"
                    + "householdContactRateMultiplierForHospitalised may not be set to be greater than 1.");
        }
        if (instance.hospitalContactRateLevelOfVariation.toUpperCase().equals("SUBPOPULATION")
                && ((instance.contactrateHospitalPatientsDailyAverage != instance.contactrateHospitalPersonnelDailyAverage) ||
                        (instance.contactrateHospitalPatientsDeviation != instance.contactrateHospitalPersonnelDeviation))) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "To allow hospital personnel and patients to differ in their mean within hospital contact rates (deviation), \n"
                    + "please set the hospitalContactRateLevelOfVariation to SUBPOPULATIONMEMBERSC or to SUBPOPULATIONMEMBERNOSC");
        }
        if ((instance.hospitalStayDaysAverage * instance.hospitalStayUnitsPerDay) < 1.) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "Since individuals stay at least one hospitalStayUnits in the hospital, the hospitalStayDaysAverage must be \n"
                    + "greater than one such unit. Please set hospitalStayDaysAverage >= (1 / hospitalStayUnitsPerDay). ");
        }
        if ((instance.numberOfHouseholds) < 2) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "number of households must be > 1 ");
        }
        if ((instance.clearingByABProbabilitiesTargetedIncreaseParameter) < 1.) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "As the model assumes that targeted antibiotics are more effective against MRSA than standard therapy, "
                    + "the clearingByABProbabilitiesTargetedIncreaseParameter must be at least 1 ");
        }
        // (The following demand may be overly strict if the mean household size is large).
        if (((instance.numberOfPersonnelPerNursingHomeInhabitant * instance.numberOfNursingHomes
                * getMax(instance.nursingHomeSizeOptions)) +
                (instance.numberOfPersonnelPerHospitalPatient * instance.numberOfHospitals)) > instance.numberOfHouseholds) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "There are potentially not enough general population people to work in a nursing home or hospital. please increase the \n"
                    + "number of households or decrease the numberOfPersonnelPerNursingHomeInhabitant / numberOfPersonnelPerHospitalPatient.");
        }
        // due to rounding in initially infected numbers, getSum(instance.fractionInitiallyInfectedPerStrain) > 1 might not be strict
        // enough to avoid errors
        if ((getSum(instance.fractionInitiallyInfectedPerStrain) * instance.numberOfHouseholds + instance.numberOfStrains) >
        		instance.numberOfHouseholds) {
            throw new ExceptionInInitializerError("\nParameter input error: \n"
                    + "The fractions initially infected per strain should not add up to more than 1.");
        }
        // Potential unchecked for problems due to parameter settings include the following:
        // - With too high hospitalization rate we run out of people that can be hospitalized.
        // -> leads to a throw from the MagicTree, through HospitalisationEvent.java.
        // - We can run out of not hospitalized general population individuals to recruit as hospital personnel.
        // -> Results in a warning by the LOGGER.
    }

    /**
     * Create a double[] array with combined mean and shape parameter of a distribution, to enable
     * distribution flexibility (CONSTANT and EXPONENTIAL have only a mean in this double[])
     * (so do POISSON and GEOMETRIC of the discrete distributions)
     * @param distribution distribution
     * @param mean mean
     * @param SD SD
     * @return double[]
     */
    private double[] combineDistributionParameters(String distribution, double mean, double SD) {
        switch (distribution.toUpperCase()) {
            case "CONSTANT":
            case "EXPONENTIAL":
            case "POISSON":
            case "GEOMETRIC":
                final double[] infectionDurationDistributionParameters1 = { mean };
                return infectionDurationDistributionParameters1;
            default:
                final double[] infectionDurationDistributionParameters2 = { mean, SD };
                return infectionDurationDistributionParameters2;
        }
    }

    public double getAfterAntibioticsAlteredSusceptibilityPeriodInDays() {
        return afterAntibioticsAlteredSusceptibilityPeriodInDays;
    }
    
    public double getAfterAntibioticsSusceptibilityMultiplier() {
        return afterAntibioticsSusceptibilityMultiplier;
    }

    public double[] getAntibioticsDailyUsageDistributionParameters() {
        return combineDistributionParameters(antibioticsUsageDistribution,
                antibioticsYearlyUsageAverage / daysPerYear, antibioticsYearlyUsageDeviation / daysPerYear);
    }

    public double getAntibioticsHospitalExtraDailyUsageAverage() {
        return antibioticsHospitalExtraYearlyUsageAverage / daysPerYear;
    }

    public double getAntibioticsHospitalExtraYearlyUsageAverage() {
        return antibioticsHospitalExtraYearlyUsageAverage;
    }

    public double getAntibioticsInfectivityPow() {
        return antibioticsInfectivityPow;
    }

    public double getAntibioticsSusceptibilityPow() {
        return antibioticsSusceptibilityPow;
    }

    public double getAntibioticsTherapyLengthInDays() {
        return antibioticsTherapyLengthInDays;
    }

    public String getAntibioticsUsageDistribution() {
        return antibioticsUsageDistribution;
    }

    public double getAntibioticsYearlyUsageAverage() {
        return antibioticsYearlyUsageAverage;
    }

    public double getAntibioticsYearlyUsageDeviation() {
        return antibioticsYearlyUsageDeviation;
    }

    public double[] getClearingByABProbabilitiesPerStrain() {
        return clearingByABProbabilitiesPerStrain;
    }

    public double getClearingByABProbabilitiesTargetedIncreaseParameter() {
        return clearingByABProbabilitiesTargetedIncreaseParameter;
    }

    public String getContactrateHospitalDistribution() {
        return contactrateHospitalDistribution;
    }

    public double getContactrateHospitalPatientsDailyAverage() {
        return contactrateHospitalPatientsDailyAverage;
    }

    public double getContactrateHospitalPatientsDeviation() {
        return contactrateHospitalPatientsDeviation;
    }

    public double[] getContactrateHospitalPatientsDistributionParameters() {
        return combineDistributionParameters(contactrateHospitalDistribution,
                contactrateHospitalPatientsDailyAverage, contactrateHospitalPatientsDeviation);
    }

    public double getContactrateHospitalPersonnelDailyAverage() {
        return contactrateHospitalPersonnelDailyAverage;
    }

    public double getContactrateHospitalPersonnelDeviation() {
        return contactrateHospitalPersonnelDeviation;
    }

    public double[] getContactrateHospitalPersonnelDistributionParameters() {
        return combineDistributionParameters(contactrateHospitalDistribution,
                contactrateHospitalPersonnelDailyAverage, contactrateHospitalPersonnelDeviation);
    }

    public double getContactrateHouseholdDailyAverage() {
        return contactrateHouseholdDailyAverage;
    }

    public double getContactrateHouseholdDeviation() {
        return contactrateHouseholdDeviation;
    }

    public String getContactrateHouseholdDistribution() {
        return contactrateHouseholdDistribution;
    }

    public double[] getContactrateHouseholdDistributionParameters() {
        return combineDistributionParameters(contactrateHouseholdDistribution,
                recalculatedContactrateHouseholdAverage(), contactrateHouseholdDeviation);
    }

    public double getContactrateNursingHomeDailyAverage() {
        return contactrateNursingHomeDailyAverage;
    }

    public double getContactrateNursingHomeDeviation() {
        return contactrateNursingHomeDeviation;
    }

    public String getContactrateNursingHomeDistribution() {
        return contactrateNursingHomeDistribution;
    }

    public double[] getContactrateNursingHomeDistributionParameters() {
        return combineDistributionParameters(contactrateNursingHomeDistribution,
                contactrateNursingHomeDailyAverage, contactrateNursingHomeDeviation);
    }

    public double getContactrateNursingHomePersonnelDailyAverage() {
        return contactrateNursingHomePersonnelDailyAverage;
    }

    public double getContactrateNursingHomePersonnelDeviation() {
        return contactrateNursingHomePersonnelDeviation;
    }

    public double[] getContactrateNursingHomePersonnelDistributionParameters() {
        return combineDistributionParameters(contactrateNursingHomeDistribution,
                contactrateNursingHomePersonnelDailyAverage, contactrateNursingHomePersonnelDeviation);
    }

    public double getContactratePopulationDailyAverage() {
        return contactratePopulationDailyAverage;
    }

    public double getContactratePopulationDeviation() {
        return contactratePopulationDeviation;
    }

    public String getContactratePopulationDistribution() {
        return contactratePopulationDistribution;
    }

    public double[] getContactratePopulationDistributionParameters() {
        return combineDistributionParameters(contactratePopulationDistribution,
                contactratePopulationDailyAverage, contactratePopulationDeviation);
    }

    public double[] getDailyHospitalisationRateDistributionParameters() {
        return combineDistributionParameters(hospitalisationRateDistribution,
                hospitalisationYearlyAverage / daysPerYear, hospitalisationRateDeviation / daysPerYear);
    }

    public double getEventsWaitPeriodInDays() {
        return eventsWaitPeriodInDays;
    }

    public double[] getExternalForceOfInfectionPerStrain() {
        return externalForceOfInfectionPerStrain;
    }

    public int getExtraOutputMaxHouseholdID() {
        return extraOutputMaxHouseholdID;
    }

    public int getExtraOutputMaxNursingHomeID() {
        return extraOutputMaxNursingHomeID;
    }

    public int getFamilyTestingDelayInDays() {
        return familyTestingDelayInDays;
    }

    public double[] getFractionInitiallyInfectedPerStrain() {
        return fractionInitiallyInfectedPerStrain;
    }

    public double getFractionOfContactsWithinWards() {
        return fractionOfContactsWithinWards;
    }

    public double getFractionOfHospitalContactsRemainingDespiteIsolation() {
        return fractionOfHospitalContactsRemainingDespiteIsolation;
    }

    public double getFractionPatientsTestedForMrsaPerDay() {
        return fractionPatientsTestedForMrsaPerDay;
    }

    public double getFractionRandomTestedForMrsaAtHospitalEntrance() {
        return fractionRandomTestedForMrsaAtHospitalEntrance;
    }

    public double getFractionRiskgroup() {
        return fractionRiskgroup;
    }

    public double getGeneralPopulationContactRateMultiplierForHospitalised() {
        return generalPopulationContactRateMultiplierForHospitalised;
    }

    public String getHospitalContactRateLevelOfVariation() {
        return hospitalContactRateLevelOfVariation;
    }

    public double getHospitalContactRateSaturatingAlfa() {
        return hospitalContactRateSaturatingAlfa;
    }

    public String getHospitalContactRateStructure() {
        return hospitalContactRateStructure;
    }

    public double getHospitalisationRateDeviation() {
        return hospitalisationRateDeviation;
    }

    public String getHospitalisationRateDistribution() {
        return hospitalisationRateDistribution;
    }

    public double getHospitalisationYearlyAverage() {
        return hospitalisationYearlyAverage;
    }

    public double getHospitalStayDaysAverage() {
        return hospitalStayDaysAverage;
    }

    public double getHospitalStayDaysDeviation() {
        return hospitalStayDaysDeviation;
    }

    public String getHospitalStayDaysDistribution() {
        return hospitalStayDaysDistribution;
    }

    /*
     * We decrease here by one unit since we add 1 unit where we schedule leaving the hospital (the minimal stay time is 1 unit)
     */
    public int getHospitalStayDurationInHospitalStayUnitsAverage() {
        return (int) hospitalStayDaysAverage * hospitalStayUnitsPerDay - 1;
    }

    public double[] getHospitalStayDurationInHospitalStayUnitsDistributionParameters() {
        return combineDistributionParameters(hospitalStayDaysDistribution,
                getHospitalStayDurationInHospitalStayUnitsAverage(), hospitalStayDaysDeviation * hospitalStayUnitsPerDay);
    }

    public double getHospitalStayUnitsPerDay() {
        return hospitalStayUnitsPerDay;
    }

    public String getHouseholdContactRateLevelOfVariation() {
        return householdContactRateLevelOfVariation;
    }

    public double getHouseholdContactRateMultiplierForHospitalised() {
        return householdContactRateMultiplierForHospitalised;
    }

    public double getHouseholdContactRateSaturatingAlfa() {
        return householdContactRateSaturatingAlfa;
    }

    public String getHouseholdContactRateStructure() {
        return householdContactRateStructure;
    }

    public double[] getHouseholdSizeProbabilities() {
        return householdSizeProbabilities;
    }

    public double[] getHouseholdSizeProbabilitiesNormalised() {
        return CollectUtils.normalize(householdSizeProbabilities);
    }

    public double[] getInfectionDurationAveragePerTypeInDays() {
        return infectionDurationAveragePerTypeInDays;
    }

    public double getInfectionDurationDeviation() {
        return infectionDurationDeviation;
    }

    public double[] getInfectionDurationDistrbutionParameters() {
        return combineDistributionParameters(infectionDurationDistribution, 1.0, infectionDurationDeviation);
        // gets multiplied by an individual and a strain dependent parameter that sets the means, so just needs shape here.
    }

    public String getInfectionDurationDistribution() {
        return infectionDurationDistribution;
    }

    public double[] getInfectionDurationIndividualProbabilitiesToBeOfType() {
        return infectionDurationIndividualProbabilitiesToBeOfType;
    }

    public double[] getInfectionProbabilitiesPerStrain() {
        return infectionProbabilitiesPerStrain;
    }

    public double getMrsaTestSensitivity() {
        return mrsaTestSensitivity;
    }

    public int getNumberOfHospitals() {
        return numberOfHospitals;
    }

    public int getNumberOfHouseholds() {
        return numberOfHouseholds;
    }

    public int getNumberOfNursingHomes() {
        return numberOfNursingHomes;
    }

    public double getNumberOfPersonnelPerHospitalPatient() {
        return numberOfPersonnelPerHospitalPatient;
    }

    public double getNumberOfPersonnelPerNursingHomeInhabitant() {
        return numberOfPersonnelPerNursingHomeInhabitant;
    }

    public int getNumberOfRepeatRuns() {
        return numberOfRepeatRuns;
    }
    
    public int getNumberOfStrains() {
        return numberOfStrains;
    }

    public int getNumberOfSusceptibleStrains() {
        return numberOfSusceptibleStrains;
    }

    public int getNumberOfWardsPerHospital() {
        return numberOfWardsPerHospital;
    }

    public double getNursinghomeAntibioticsExtraDailyUsageAverage() {
        return nursinghomeAntibioticsExtraYearlyUsageAverage / daysPerYear;
    }

    public double getNursinghomeAntibioticsExtraYearlyUsageAverage() {
        return nursinghomeAntibioticsExtraYearlyUsageAverage;
    }

    public String getNursingHomeContactRateLevelOfVariation() {
        return nursingHomeContactRateLevelOfVariation;
    }
    
    public double getNursinghomeContactratePopulationMultiplier() {
        return nursinghomeContactratePopulationMultiplier;
    }

    public double getNursingHomeContactRateSaturatingAlfa() {
        return nursingHomeContactRateSaturatingAlfa;
    }

    public String getNursingHomeContactRateStructure() {
        return nursingHomeContactRateStructure;
    }

    public double getNursinghomeHospitalisationExtraDailyAverage() {
        return nursinghomeHospitalisationExtraYearlyAverage / daysPerYear;
    }

    public double getNursinghomeHospitalisationExtraYearlyAverage() {
        return nursinghomeHospitalisationExtraYearlyAverage;
    }

    public int[] getNursingHomeSizeOptions() {
        return nursingHomeSizeOptions;
    }

    public double[] getNursingHomeSizeProbabilities() {
        return nursingHomeSizeProbabilities;
    }

    public double[] getNursingHomeSizeProbabilitiesNormalised() {
        return CollectUtils.normalize(nursingHomeSizeProbabilities);
    }

    public long getOgInterval() {
        return calcDurationAsLong(outputIntervalUnit, outputInterval);
    }
    
    public long getSimDuration() {
        return calcDurationAsLong(simulationDurationUnit, simulationDuration);
    }
    
    /**
     * Calculate a duration in TimeUtils units from two user given parameters.
     * @param durationUnit A string giving the duration unit (years, months, weeks or days)
     * @param durationInDurationUnit A double stating the duration in durationUnit.
     * @return a duration in TimeUtils units (long)
     */
    private long calcDurationAsLong(String durationUnit, double durationInDurationUnit) {
        double duryrs = 0.0;
        double durmnths = 0.0;
        double durwks = 0.0;
        double durdys = 0.0;
        switch (durationUnit.toUpperCase()) {
            case "YEARS":
                duryrs = durationInDurationUnit;
                break;
            case "MONTHS":
                durmnths = durationInDurationUnit;
                break;
            case "WEEKS":
                durwks = durationInDurationUnit;
                break;
            case "DAYS":
                durdys = durationInDurationUnit;
                break;
        }
        // each year has 365.2425 days, and 12 equal sized months
        double durationInDays = daysPerYear * duryrs + (daysPerYear / 12) * durmnths + 7 * durwks + durdys;
        return TimeUtilsDays.asDuration(durationInDays, 0, 0, 0);
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public double getOutputInterval() {
        return outputInterval;
    }

    public String getOutputIntervalUnit() {
        return outputIntervalUnit;
    }

    public double getProbabilityForNewContactRateAtNewHospitalisation() {
        return probabilityForNewContactRateAtNewHospitalisation;
    }

    public Long getSeed() {
        return seed;
    }

    public double getSimulationDuration() {
        return simulationDuration;
    }

    public String getSimulationDurationUnit() {
        return simulationDurationUnit;
    }

    public double getTimeDelayForMrsaTestResultsDaysAverage() {
        return timeDelayForMrsaTestResultsDaysAverage;
    }

    public double getTimeDelayForMrsaTestResultsDeviation() {
        return timeDelayForMrsaTestResultsDeviation;
    }

    public String getTimeDelayForMrsaTestResultsDistribution() {
        return timeDelayForMrsaTestResultsDistribution;
    }

    public int getTimeDelayForMrsaTestResultsInHospitalStayUnitsAverage() {
        return (int) timeDelayForMrsaTestResultsDaysAverage * hospitalStayUnitsPerDay;
    }

    public double[] getTimeDelayForMrsaTestResultsInHospitalStayUnitsDistributionParameters() {
        return combineDistributionParameters(timeDelayForMrsaTestResultsDistribution,
                getTimeDelayForMrsaTestResultsInHospitalStayUnitsAverage(), 
                timeDelayForMrsaTestResultsDeviation * hospitalStayUnitsPerDay);
    }

    public double[] getTimeDelayForMrsaTestResultsParameters() {
        return combineDistributionParameters(timeDelayForMrsaTestResultsDistribution,
                timeDelayForMrsaTestResultsDaysAverage, timeDelayForMrsaTestResultsDeviation);
    }

    public String getTimeStamp() {
        return timeStamp;
    }

	public boolean isFamilyTestingFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn() {
        return familyTestingFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn;
    }

    public boolean isFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn() {
        return foundMrsaPositiveTargetedAntibioticsTreatmentIsOn;
    }

    public boolean isGiveOutputPerNursingHomeAndHospital() {
        return giveOutputPerNursingHomeAndHospital;
    }

    public boolean isSearchAndDestroyIsOn() {
        return searchAndDestroyIsOn;
    }
    
    /*
     * Recalculation of the average household contact rate. Depending on the within household
     * contact rate structure, the set contact rate is multiplied depending on the number of household members.
     * This is compensated for here.
     */
    private double recalculatedContactrateHouseholdAverage() {
        final int[] householdsizes = new int[householdSizeProbabilities.length];
        for (int i = 0; i < householdSizeProbabilities.length; ++i) {
            householdsizes[i] = i + 1;
        }
        return contactrateHouseholdDailyAverage / calcAverageHHContactRateMultiplication(
                householdContactRateStructure, householdSizeProbabilities, householdsizes, householdContactRateSaturatingAlfa);
    }
    
    /*
     * calculation of the average household contact rate multiplication. First calculates the probability for
     *  a person to be in a certain size household, then multiplies this by the number of contacts (i.e. others
     *  in the household) to get the average contact rate multiplication.
     */
    private double calcAverageHHContactRateMultiplication(
            String contactRateStructure, double[] SizeProbabilities, int[] sizes, double saturatingAlfa) {
        double[] probstebeinhouseholdsize = SizeProbabilities.clone();
        for (int i = 0; i < probstebeinhouseholdsize.length; i++) {
            probstebeinhouseholdsize[i] = probstebeinhouseholdsize[i] * sizes[i];
        }
        probstebeinhouseholdsize = CollectUtils.normalize(probstebeinhouseholdsize);
        double averageHHContactRateMultiplication = 0.0;
        for (int i = 0; i < probstebeinhouseholdsize.length; i++) {
            double mult = 0.0;
            switch (contactRateStructure.toUpperCase()) {
                case "SATURATING":
                    mult = sizes[i] > 1 ? saturatingAlfa * Math.log(sizes[i] - 1) / (sizes[i] - 1) : 0.0;
                    break;
                case "LINEAR":
                    mult = sizes[i] - 1;
                    break;
                case "CONSTANT":
                    mult = sizes[i] > 1 ? 1.0 : 0.0;
            }
            averageHHContactRateMultiplication += probstebeinhouseholdsize[i] * mult;
        }
        return averageHHContactRateMultiplication;
    }

    private double getMax(int[] array) {
        // finds the highest value
        double max = array[0];
        for (int counter = 1; counter < array.length; counter++) {
            if (array[counter] > max) {
                max = array[counter];
            }
        }
        return max;
    }
    
    private double getSum(double[] array) {
        double total = array[0];
        for (int counter = 1; counter < array.length; counter++) {
            total += array[counter];
        }
        return total;
    }
    
    @Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ParameterInput \noutputFileName=").append(outputFileName)
				.append("\ngiveOutputPerNursingHomeAndHospital=").append(giveOutputPerNursingHomeAndHospital)
				.append("\nextraOutputMaxHouseholdID=").append(extraOutputMaxHouseholdID)
				.append("\nextraOutputMaxNursingHomeID=").append(extraOutputMaxNursingHomeID)
				.append("\nnumberOfRepeatRuns=").append(numberOfRepeatRuns).append("\nseed=").append(seed)
				.append("\nnumberOfHouseholds=").append(numberOfHouseholds).append("\nhouseholdSizeProbabilities=")
				.append(Arrays.toString(householdSizeProbabilities)).append("\nsimulationDurationUnit=")
				.append(simulationDurationUnit).append("\nsimulationDuration=").append(simulationDuration)
				.append("\noutputIntervalUnit=").append(outputIntervalUnit).append("\noutputInterval=")
				.append(outputInterval).append("\nnumberOfStrains=").append(numberOfStrains)
				.append("\nnumberOfSusceptibleStrains=").append(numberOfSusceptibleStrains)
				.append("\nfractionInitiallyInfectedPerStrain=")
				.append(Arrays.toString(fractionInitiallyInfectedPerStrain)).append("\nfractionRiskgroup=")
				.append(fractionRiskgroup).append("\nexternalForceOfInfectionPerStrain=")
				.append(Arrays.toString(externalForceOfInfectionPerStrain)).append("\ninfectionProbabilitiesPerStrain=")
				.append(Arrays.toString(infectionProbabilitiesPerStrain))
				.append("\nclearingByABProbabilitiesPerStrain=")
				.append(Arrays.toString(clearingByABProbabilitiesPerStrain)).append("\nantibioticsUsageDistribution=")
				.append(antibioticsUsageDistribution).append("\nantibioticsYearlyUsageAverage=")
				.append(antibioticsYearlyUsageAverage).append("\nantibioticsYearlyUsageDeviation=")
				.append(antibioticsYearlyUsageDeviation).append("\nantibioticsTherapyLengthInDays=")
				.append(antibioticsTherapyLengthInDays).append("\nantibioticsInfectivityPow=")
				.append(antibioticsInfectivityPow).append("\nantibioticsSusceptibilityPow=")
				.append(antibioticsSusceptibilityPow).append("\nafterAntibioticsAlteredSusceptibilityPeriodInDays=")
				.append(afterAntibioticsAlteredSusceptibilityPeriodInDays)
				.append("\nafterAntibioticsSusceptibilityMultiplier=").append(afterAntibioticsSusceptibilityMultiplier)
				.append("\ninfectionDurationDistribution=").append(infectionDurationDistribution)
				.append("\ninfectionDurationDeviation=").append(infectionDurationDeviation)
				.append("\ninfectionDurationIndividualProbabilitiesToBeOfType=")
				.append(Arrays.toString(infectionDurationIndividualProbabilitiesToBeOfType))
				.append("\ninfectionDurationAveragePerTypeInDays=")
				.append(Arrays.toString(infectionDurationAveragePerTypeInDays))
				.append("\ncontactratePopulationDistribution=").append(contactratePopulationDistribution)
				.append("\ncontactratePopulationDailyAverage=").append(contactratePopulationDailyAverage)
				.append("\ncontactratePopulationDeviation=").append(contactratePopulationDeviation)
				.append("\nhouseholdContactRateLevelOfVariation=").append(householdContactRateLevelOfVariation)
				.append("\nhouseholdContactRateStructure=").append(householdContactRateStructure)
				.append("\nhouseholdContactRateSaturatingAlfa=").append(householdContactRateSaturatingAlfa)
				.append("\ncontactrateHouseholdDistribution=").append(contactrateHouseholdDistribution)
				.append("\ncontactrateHouseholdDailyAverage=").append(contactrateHouseholdDailyAverage)
				.append("\ncontactrateHouseholdDeviation=").append(contactrateHouseholdDeviation)
				.append("\nnumberOfHospitals=").append(numberOfHospitals).append("\nnumberOfWardsPerHospital=")
				.append(numberOfWardsPerHospital).append("\nfractionOfContactsWithinWards=")
				.append(fractionOfContactsWithinWards).append("\nhospitalisationRateDistribution=")
				.append(hospitalisationRateDistribution).append("\nhospitalisationYearlyAverage=")
				.append(hospitalisationYearlyAverage).append("\nhospitalisationRateDeviation=")
				.append(hospitalisationRateDeviation).append("\nhospitalStayUnitsPerDay=")
				.append(hospitalStayUnitsPerDay).append("\nhospitalStayDaysDistribution=")
				.append(hospitalStayDaysDistribution).append("\nhospitalStayDaysAverage=")
				.append(hospitalStayDaysAverage).append("\nhospitalStayDaysDeviation=")
				.append(hospitalStayDaysDeviation).append("\nhospitalContactRateLevelOfVariation=")
				.append(hospitalContactRateLevelOfVariation).append("\nhospitalContactRateStructure=")
				.append(hospitalContactRateStructure).append("\nhospitalContactRateSaturatingAlfa=")
				.append(hospitalContactRateSaturatingAlfa).append("\ncontactrateHospitalDistribution=")
				.append(contactrateHospitalDistribution).append("\ncontactrateHospitalPatientsDailyAverage=")
				.append(contactrateHospitalPatientsDailyAverage).append("\ncontactrateHospitalPatientsDeviation=")
				.append(contactrateHospitalPatientsDeviation)
				.append("\nprobabilityForNewContactRateAtNewHospitalisation=")
				.append(probabilityForNewContactRateAtNewHospitalisation)
				.append("\nhouseholdContactRateMultiplierForHospitalised=")
				.append(householdContactRateMultiplierForHospitalised)
				.append("\ngeneralPopulationContactRateMultiplierForHospitalised=")
				.append(generalPopulationContactRateMultiplierForHospitalised)
				.append("\nantibioticsHospitalExtraYearlyUsageAverage=")
				.append(antibioticsHospitalExtraYearlyUsageAverage).append("\nnumberOfPersonnelPerHospitalPatient=")
				.append(numberOfPersonnelPerHospitalPatient).append("\ncontactrateHospitalPersonnelDailyAverage=")
				.append(contactrateHospitalPersonnelDailyAverage).append("\ncontactrateHospitalPersonnelDeviation=")
				.append(contactrateHospitalPersonnelDeviation).append("\nnumberOfNursingHomes=")
				.append(numberOfNursingHomes).append("\nnursingHomeSizeOptions=")
				.append(Arrays.toString(nursingHomeSizeOptions)).append("\nnursingHomeSizeProbabilities=")
				.append(Arrays.toString(nursingHomeSizeProbabilities))
				.append("\nnursingHomeContactRateLevelOfVariation=").append(nursingHomeContactRateLevelOfVariation)
				.append("\nnursingHomeContactRateStructure=").append(nursingHomeContactRateStructure)
				.append("\nnursingHomeContactRateSaturatingAlfa=").append(nursingHomeContactRateSaturatingAlfa)
				.append("\ncontactrateNursingHomeDistribution=").append(contactrateNursingHomeDistribution)
				.append("\ncontactrateNursingHomeDailyAverage=").append(contactrateNursingHomeDailyAverage)
				.append("\ncontactrateNursingHomeDeviation=").append(contactrateNursingHomeDeviation)
				.append("\nnursinghomeContactratePopulationMultiplier=")
				.append(nursinghomeContactratePopulationMultiplier)
				.append("\nnursinghomeAntibioticsExtraYearlyUsageAverage=")
				.append(nursinghomeAntibioticsExtraYearlyUsageAverage)
				.append("\nnursinghomeHospitalisationExtraYearlyAverage=")
				.append(nursinghomeHospitalisationExtraYearlyAverage)
				.append("\nnumberOfPersonnelPerNursingHomeInhabitant=")
				.append(numberOfPersonnelPerNursingHomeInhabitant)
				.append("\ncontactrateNursingHomePersonnelDailyAverage=")
				.append(contactrateNursingHomePersonnelDailyAverage)
				.append("\ncontactrateNursingHomePersonnelDeviation=").append(contactrateNursingHomePersonnelDeviation)
				.append("\nsearchAndDestroyIsOn=").append(searchAndDestroyIsOn).append("\nfamilyTestingDelayInDays=")
				.append(familyTestingDelayInDays).append("\nfractionRandomTestedForMrsaAtHospitalEntrance=")
				.append(fractionRandomTestedForMrsaAtHospitalEntrance).append("\nfractionPatientsTestedForMrsaPerDay=")
				.append(fractionPatientsTestedForMrsaPerDay).append("\ntimeDelayForMrsaTestResultsDistribution=")
				.append(timeDelayForMrsaTestResultsDistribution).append("\ntimeDelayForMrsaTestResultsDaysAverage=")
				.append(timeDelayForMrsaTestResultsDaysAverage).append("\ntimeDelayForMrsaTestResultsDeviation=")
				.append(timeDelayForMrsaTestResultsDeviation).append("\nmrsaTestSensitivity=")
				.append(mrsaTestSensitivity).append("\nfractionOfHospitalContactsRemainingDespiteIsolation=")
				.append(fractionOfHospitalContactsRemainingDespiteIsolation)
				.append("\nfamilyTestingFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn=")
				.append(familyTestingFoundMrsaPositiveTargetedAntibioticsTreatmentIsOn)
				.append("\nfoundMrsaPositiveTargetedAntibioticsTreatmentIsOn=")
				.append(foundMrsaPositiveTargetedAntibioticsTreatmentIsOn)
				.append("\nclearingByABProbabilitiesTargetedIncreaseParameter=")
				.append(clearingByABProbabilitiesTargetedIncreaseParameter).append("\neventsWaitPeriodInDays=")
				.append(eventsWaitPeriodInDays).append("\ntimeStamp=").append(timeStamp).append("\ndaysPerYear=")
				.append(daysPerYear);
		return builder.toString();
	}
}
